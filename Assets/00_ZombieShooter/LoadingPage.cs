﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingPage : MonoBehaviour
{
	AsyncOperation _async;
	[SerializeField] Image _ProgressBarSlider;
	public Text txt_Loading1, txt_Loading;
	public Text Text_Hint;
	public string[] _sHints;
	void Awake ()
	{
		AudioListener.volume = 0;//StaticVariables.iSoundStatus;
	}

	void Start ()
	{
		Invoke ("RequestToLoadNextScene", 2f);
		_ProgressBarSlider.fillAmount = 0;
		txt_Loading1.text	= "0 %";
		int _strval = Random.Range (0, _sHints.Length);
		Text_Hint.text = "* " + _sHints[_strval];
	}

	void RequestToLoadNextScene ()
	{
		
		StartCoroutine (ieRequestToLoadNextScene ());
	}

	IEnumerator ieRequestToLoadNextScene ()
	{
		
		if (StaticVariables._sSceneToLoad == "")
			yield break;
		
		_async	= Application.LoadLevelAsync (StaticVariables._sSceneToLoad);
		yield return _async; 
	}

	void Update ()
	{
		
		if (_async != null)
		{
			_ProgressBarSlider.fillAmount = _async.progress + 0.1f;
			txt_Loading1.text	= "" + (int)(_ProgressBarSlider.fillAmount * 100) + " %";
//			SetLoadingText ();
		}
	}

	int length = 0;
	string _str	= "Loading";
	float timer	= 0;

	void SetLoadingText ()
	{
		timer += Time.deltaTime;
		if (timer >= 0.1f)
		{
			timer = 0;
			length++;
			if (length == 1)
				_str = "Loading.";
			else if (length == 2)
				_str = "Loading..";
			else if (length == 3)
				_str = "Loading...";
			else if (length == 4)
			{
				length = 0;
				_str = "Loading";
			}
			txt_Loading.text	= _str;
		}
	}
}
