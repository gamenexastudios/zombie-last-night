thank u for purchasing fps weapons!

in order to play the demo scene u need to unzip the projectsettings folder provided in the asset folder and overwrite your default ones.
or use the asset to build your own ;)

demoscene controls:

WASD to move
SPACE to jump
LEFT SHIFT to run
LEFT MOUSE to fire
RIGHT MOUSE to aim
MIDDLE MOUSE SCROLL to switch weapon
F action
G grenadetoss
t meleeweapon
C crouch/uncrouch
r to reload
(or use a XBOX controller)

press jump to enter a ladder from the bottom, when entering a ladder from top this is not necessary.

if the GUI does not display correctly set the game aspect ratio to 16:9
the shaders included are created with shaderforge for forward rendering and look best in linear color space.

note that the scripts and demoscene provided are for demonstration purpose ,
this is not intended to be a full FPS kit but should give u an idea on how to set the weapons up.




for questions mail to 3dmaesen@gmail.com

have fun!

