﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MySystem;


public class CameraControllesAssaults : MonoBehaviour
{
	public static CameraControllesAssaults myScript;

	
	public float _minXAngle, _maxXAngle, _minYAngle, _maxYAngle;
	public float sensitivityX, sensitivityY;
	Camera _targetCamera;
	public static	float _targetFieldOfView, speed;
	public float rotationY, rotationX;
	public Vector3 firstPoint, secondPoint, FinalRotWithLerp;
	public float extraXVal, extraYVal, xTemp = 0, yTemp = 0, downTime, scopeVal, tempRotationY, tempRotationX;
	bool zoomMode;
	public int _targetZoomVal;
	public  static int CamerafingerId = -1;
	
	public GUITexture Btn_Zoom;
	public GUITexture Btn_ZoomSlider;
	public bool Ignorenow;

	public GraphicRaycaster _Raycaster;
	public EventSystem _Eventsystemref;
	PointerEventData _pointereventdata;

	void Awake ()
	{
		myScript = this;
	}

	void Start ()
	{
		Vector3 _new = new Vector3 (-rotationY + extraXVal, rotationX + extraYVal, 0);
		transform.localEulerAngles = _new; 
		_targetFieldOfView = 60;
		speed = 20;
		_targetCamera = this.GetComponent<Camera> ();
		Input.multiTouchEnabled = true;

		_pointereventdata = new PointerEventData (_Eventsystemref);
	}

	void Update ()
	{
        if (!GameManager._instance)
            return;

		if (GameManager._instance.m_GameState != GameManager.e_GameState.GamePlay)
			return;

		if (Time.timeScale == 0 || Ignorenow)
			return;

        //		SetFieldOfView ();

#if UNITY_EDITOR

        if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButtonDown(0))
            {
                //       fingerId = Input.GetTouch (0).fingerId;
                firstPoint = Input.mousePosition;
                xTemp = rotationX;
                yTemp = rotationY;

                Firstposition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
            if (Input.GetMouseButton(0))
            {
                secondPoint = Input.mousePosition;
                float _dividerVal = 80;
                //						if (genericShooter.myScript.isCameraZoomed)
                //						{
                //							_dividerVal = 300;
                //						}

                float xVAL = xTemp + ((secondPoint.x - firstPoint.x) * 180 / Screen.width * (_targetFieldOfView / _dividerVal));
                float yVAL = yTemp + ((secondPoint.y - firstPoint.y) * 90 / Screen.height * (_targetFieldOfView / _dividerVal));


                rotationX = xVAL;
                //						rotationX = Mathf.Clamp (xVAL, minXAngle + targetFieldOfView / dividerVal, maxXAngle + targetFieldOfView / dividerVal);
                rotationY = Mathf.Clamp(yVAL, _minYAngle + _targetFieldOfView / _dividerVal, _maxYAngle + _targetFieldOfView / _dividerVal);
            }

        }
#endif


#if UNITY_ANDROID

        if (Input.touchCount> 0)
        {
            for (int i = 0; i < Input.touches.Length; i++)
            {
				/*
				if (Input.GetTouch (i).phase == TouchPhase.Moved || Input.GetTouch(i).phase==TouchPhase.Stationary)//&&  Input.GetTouch(i).fingerId==fingerId)
					{
					if(CamerafingerId==-1 && Input.GetTouch(i).fingerId!=Virtualjoystick.Fingerid)//  && Input.GetTouch(i).fingerId!=Virtualjoystick.Fingerid1)
						{	
							CamerafingerId = Input.GetTouch (i).fingerId;
							firstPoint = Input.GetTouch (i).position;
							xTemp = rotationX;
							yTemp = rotationY;
							Debug.Log("Camera Id :: "+CamerafingerId);


						}

					if( CamerafingerId!=-1 && Input.GetTouch(i).fingerId==CamerafingerId )		//CamerafingerId!=-1 &&					
						{
							secondPoint = Input.GetTouch (i).position;
							float xVAL = xTemp + ((secondPoint.x - firstPoint.x) * 180 / Screen.width * (_targetFieldOfView / 40));
							float yVAL = yTemp + ((secondPoint.y - firstPoint.y) * 90 / Screen.height * (_targetFieldOfView / 40));
							rotationX = xVAL;
							rotationY = Mathf.Clamp (yVAL, _minYAngle + _targetFieldOfView / 40, _maxYAngle + _targetFieldOfView / 20);
							Debug.Log("Camera Id after:: "+CamerafingerId);

						}
					}
										
					if (CamerafingerId!=-1  && (Input.GetTouch (i).phase == TouchPhase.Ended || Input.GetTouch (i).phase == TouchPhase.Canceled )) //&& Input.GetTouch(i).fingerId!=Virtualjoystick.Fingerid1  
					{
						CamerafingerId=-1;
					}	
				*/

				if (Input.GetTouch (i).phase == TouchPhase.Began  || Input.GetTouch (i).phase == TouchPhase.Stationary || Input.GetTouch (i).phase == TouchPhase.Moved )//&&  Input.GetTouch(i).fingerId==fingerId)
				{
                        
                    _pointereventdata.position = Input.GetTouch (i).position;
					//Raycast using the Graphics Raycaster and mouse click position
					List<RaycastResult> results = new List<RaycastResult> ();
                       
                    _Raycaster.Raycast(_pointereventdata, results);

                    //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
                    //Ignorevalue=-1;

                    for (int j=0;j<results.Count;j++)
                    {  
                        if (results[j].gameObject.transform.name.Contains("Joystick"))
						{
							Ignorevalue=i;
							break;
						}
					}

                /*foreach (RaycastResult result in results)
                {
                    if(result.gameObject.transform.name.Contains("Joystick"))
                    {
                        Ignorevalue=i;
                    }
                }
                */
                    
            }
				if(i!=Ignorevalue)
				{
					rotationX+=Input.GetTouch (i).deltaPosition.x *(_targetFieldOfView / 40)*(Input.touchCount/2f);
					rotationY += Input.GetTouch (i).deltaPosition.y * (_targetFieldOfView / 40)*(Input.touchCount/2f);
            }
                

            if (Ignorevalue==i && Ignorevalue>=0 && (Input.GetTouch (i).phase == TouchPhase.Ended || Input.GetTouch (i).phase == TouchPhase.Canceled ))//&&  Input.GetTouch(i).fingerId==fingerId)
			{
				Ignorevalue=-1;
                    
            }
               

            rotationY = Mathf.Clamp (rotationY, _minYAngle + _targetFieldOfView / 40, _maxYAngle + _targetFieldOfView / 20); //40
                
			}
		}

		#endif
			
		extraXVal = ((Mathf.Sin (Time.time)) / (100 - 0));
        extraYVal = ((Mathf.Cos (Time.time)) / (100 - 0));
        FinalRotWithLerp.x = Mathf.Lerp (FinalRotWithLerp.x, rotationX, Time.deltaTime * 2);
        FinalRotWithLerp.y = Mathf.Lerp (FinalRotWithLerp.y, rotationY, Time.deltaTime * 2);
        FinalRotWithLerp.z = 0;
        Vector3 _new = new Vector3 ((-FinalRotWithLerp.y + extraXVal)/3, (FinalRotWithLerp.x + extraYVal) / 3, 0);
        transform.localEulerAngles = _new;
    }

	int Ignorevalue = -1;
	float speedvalue = 0.5f;
	public Texture Tex_Zoom1;
	public Texture Tex_Zoom2;
	Vector2 Firstposition;
	Vector2 checkmagnitude;
}
