﻿using UnityEngine;
using System.Collections;

public class TrooperNPC : MonoBehaviour {

	public float wanderRadius;
	public float wanderTimer;
	public float hitpoints = 100f;
	public Transform spawnobject;
	private Transform target = null;
	private UnityEngine.AI.NavMeshAgent agent;
	public float walkspeed = 2f;
	public float runspeed = 5f;
	private float timer;

	public float walltreshold = 2f;

	Animator animator;
	public Transform lefthandtarget;
	public LayerMask mask;
	private Vector3 hitreactionVector;
	private AudioSource myaudio;
	public AudioClip[] hurtSounds; 
	private GameObject player;
	public float viewangle = 60f;
	public float viewdistance = 20f;
	public float attackdistance = 1.5f;
	private Vector3 startposition;
	private float oldhitpoints;
	private float lookweight = 0f;
	void Awake ()
	{

		animator = GetComponent<Animator>();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		timer = wanderTimer;
		animator = transform.GetComponent<Animator>();
		player = GameObject.FindGameObjectWithTag("Player");
		myaudio = GetComponent<AudioSource>();

	}
	void Start()
	{
		startposition = transform.position;
		oldhitpoints = hitpoints;
	}

	void Update ()
	{

		Vector3 velocity = agent.velocity;

		//Vector3 localvelocity = transform.InverseTransformDirection(velocity);
		//animator.SetFloat("ver",localvelocity.z);
		//animator.SetFloat("speed",velocity.magnitude);
		if (oldhitpoints != hitpoints)
		{
			animator.SetBool("gethit", true);
			oldhitpoints = hitpoints;
		}
		else
		{
			animator.SetBool("gethit", false);
		}


		if (hitpoints <= 0)
		{
			Instantiate(spawnobject, transform.position, transform.rotation);
			Collider[] colliders = Physics.OverlapSphere(hitreactionVector,1f);

			foreach (Collider didhit in colliders) 
			{
				if (didhit.GetComponent<Rigidbody>() != null)
				{
					
					Rigidbody rb = didhit.GetComponent<Rigidbody>();
					Vector3 direction = rb.transform.position - hitreactionVector;
					rb.AddForceAtPosition(direction.normalized * 2000f,hitreactionVector);

				}
			}
			Destroy (gameObject);
		}
		else
		{
			Vector3 targetDir = player.transform.position + new Vector3(0f,.5f,0f) - transform.position;
			Vector3 forward = transform.forward;
			float angle = Vector3.Angle(targetDir, forward);


			if (angle < viewangle && targetDir.magnitude <= viewdistance)
			{
				Ray lookray = new Ray (transform.position, targetDir);
				RaycastHit playerhit = new RaycastHit();
				//Debug.DrawRay(transform.position, targetDir, Color.green);
				if (Physics.Raycast(lookray,out playerhit, viewdistance,mask))
				{
					if (playerhit.transform.tag == "Player") 
					{
						agent.Stop();
						agent.updateRotation = false;
						Vector3 relativePos = ( playerhit.transform.position - transform.position);
						lookweight = 1f;
						Quaternion lookrotation = Quaternion.LookRotation(relativePos,Vector3.up);
						lookrotation.x = 0;
						lookrotation.z = 0;
						transform.rotation = Quaternion.Lerp(transform.rotation,lookrotation,Time.deltaTime * 3f);
					}
					else
					{
						dowander();
					}
				}
				else
				{
					dowander();
				}

			}
			else
			{
				dowander();
			}

		}
	}

	
	void dowander()
	{
		agent.Resume();
		//ray front
		lookweight = 0f;
		Vector3 wantedvectorfront = transform.forward;
		Ray rayfront = new Ray (transform.position + new Vector3(0f,1f,0f), wantedvectorfront);
		RaycastHit hitfront = new RaycastHit();
		//ray right
		Vector3 wantedvectorright = transform.InverseTransformDirection(transform.right);
		Ray rayright = new Ray (transform.position + new Vector3(0f,1f,0f), wantedvectorright);
		RaycastHit hitright = new RaycastHit();
		//rayleft
		Vector3 wantedvectorleft = transform.InverseTransformDirection(-transform.right);
		Ray rayleft = new Ray (transform.position + new Vector3(0f,1f,0f), wantedvectorleft);
		RaycastHit hitleft = new RaycastHit();

		if (Physics.Raycast(rayfront,out hitfront, walltreshold,mask))
		{
			if (Physics.Raycast(rayright,out hitright, walltreshold,mask))
			{
				Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,-50f,0f);
				transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);
			}
			else
			{
				agent.updateRotation = false;
				Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,50f,0f);
				transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);
			}

			agent.updateRotation = false;
		}
		if (Physics.Raycast(rayfront,out hitfront, walltreshold,mask) && (Physics.Raycast(rayright,out hitright, walltreshold,mask)))
		{

			agent.updateRotation = false;
			Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,-50,0f);
			transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);


		}
		else if (Physics.Raycast(rayfront,out hitfront, walltreshold,mask) && (Physics.Raycast(rayleft,out hitleft, walltreshold,mask)))
		{

			agent.updateRotation = false;
			Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,50f,0f);
			transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);

		}
		else
		{

			agent.updateRotation = true;
		}




		Vector3 localvelocity = transform.InverseTransformDirection(agent.velocity);
		animator.SetFloat("speed", localvelocity.magnitude);
		//animator.SetFloat("Hor", localvelocity.x / walkspeed);
		timer += Time.deltaTime;
		agent.speed = walkspeed;
		if (timer >= wanderTimer) 
		{
			Vector3 newPos = RandomNavSphere(startposition, wanderRadius, -1);
			agent.SetDestination(newPos);
			timer = 0;
		}

	}
	void OnAnimatorMove()
	{
		agent.velocity = animator.deltaPosition / Time.deltaTime;
	}
	void OnAnimatorIK()
	{
		//animator.SetIKPositionWeight(AvatarIKGoal.LeftHand,1);
		//animator.SetIKPosition(AvatarIKGoal.LeftHand,lefthandtarget.position);
		animator.SetLookAtPosition (target.transform.position);
		animator.SetLookAtWeight (lookweight, 0.7f, 1f, 1f);
	}
	public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
	{
		Vector3 randDirection = Random.insideUnitSphere * dist;

		randDirection += origin;
		UnityEngine.AI.NavMeshHit navHit;

		UnityEngine.AI.NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);

		return navHit.position;
	}
	void Damage (float damage) 
	{

		if (!myaudio.isPlaying && hitpoints >= 0)
		{

			int n = Random.Range(1,hurtSounds.Length);
			myaudio.clip = hurtSounds[n];
			myaudio.pitch = 0.9f + 0.1f *Random.value;
			myaudio.Play();
			hurtSounds[n] = hurtSounds[0];
			hurtSounds[0] = myaudio.clip;
		}
		hitpoints = hitpoints - damage;


	}
	void hitvector(Vector3 davector)
	{
		hitreactionVector = davector;
	}
}
