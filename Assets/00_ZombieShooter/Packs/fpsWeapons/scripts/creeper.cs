﻿using UnityEngine;
using System.Collections;

public class creeper : MonoBehaviour {

	public float wanderRadius;
	public float wanderTimer;
	public float hitpoints = 100f;
	public Transform spawnobject;
	private Transform target;
	private UnityEngine.AI.NavMeshAgent agent;
	public float walkspeed = 2f;
	public float runspeed = 5f;
	private float timer;
	private Animator animator;
	private GameObject player;
	public float viewangle = 60f;
	public float viewdistance = 20f;
	public float attackdistance = 1.5f;
	private AudioSource myaudio;
	public AudioClip[] hurtSounds; 
	private float oldhitpoints;
	public LayerMask mask;
	public float walltreshold = 2f;
	private Vector3 hitreactionVector;
	private Vector3 startposition;
	void Awake ()
	{

		animator = GetComponent<Animator>();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		timer = wanderTimer;
		animator = transform.GetComponent<Animator>();
		player = GameObject.FindGameObjectWithTag("Player");
		myaudio = GetComponent<AudioSource>();

	}
	void Start()
	{
		startposition = transform.position;
		oldhitpoints = hitpoints;
	}

	// Update is called once per frame
	void Update () 
	{
		Vector3 velocity = agent.velocity;

		//Vector3 localvelocity = transform.InverseTransformDirection(velocity);
		//animator.SetFloat("ver",localvelocity.z);
		animator.SetFloat("speed",velocity.magnitude / walkspeed);
		if (oldhitpoints != hitpoints)
		{
			animator.SetBool("gethit", true);
			oldhitpoints = hitpoints;
		}
		else
		{
			animator.SetBool("gethit", false);
		}
		if (hitpoints <= 0)
		{
			Instantiate(spawnobject, transform.position, transform.rotation);
			Collider[] colliders = Physics.OverlapSphere(hitreactionVector,1f);

			foreach (Collider didhit in colliders) 
			{
				if (didhit.GetComponent<Rigidbody>() != null)
				{

					Rigidbody rb = didhit.GetComponent<Rigidbody>();
					Vector3 direction = rb.transform.position - hitreactionVector;
					rb.AddForceAtPosition(direction.normalized * 120f,hitreactionVector);

				}
			}
			Destroy (gameObject);
		}
		else
		{
			

			if (player != null) {
				Vector3 targetDir = player.transform.position + new Vector3 (0f, .5f, 0f) - transform.position;
				Vector3 forward = transform.forward;
				float angle = Vector3.Angle (targetDir, forward);


				if (angle < viewangle && targetDir.magnitude <= viewdistance) {
					


					if (targetDir.magnitude <= attackdistance) {
						//attack
						agent.Stop ();
						animator.SetBool ("attack", true);
					} 
					else 
					{
						agent.speed = runspeed;
						//agent.updateRotation = true ;
						agent.Resume();
						agent.SetDestination (player.transform.position);
						Quaternion lookrotation = Quaternion.LookRotation (targetDir, Vector3.up);
						lookrotation.x = 0;
						lookrotation.z = 0;
						transform.rotation = Quaternion.Lerp (transform.rotation, lookrotation, Time.deltaTime * 3f);
						animator.SetBool ("attack", false);
					}

				}
				else 
				{
					dowander ();
					animator.SetBool ("attack", false);
				}



			}
			else 
			{
				dowander ();
				animator.SetBool ("attack", false);
			}
		}
		
	}

	public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
		Vector3 randDirection = Random.insideUnitSphere * dist;

		randDirection += origin;

		UnityEngine.AI.NavMeshHit navHit;

		UnityEngine.AI.NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);

		return navHit.position;
	}
	void Damage (float damage) 
	{

		if (!myaudio.isPlaying && hitpoints >= 0)
		{
			
			int n = Random.Range(1,hurtSounds.Length);
			myaudio.clip = hurtSounds[n];
			myaudio.pitch = 0.9f + 0.1f *Random.value;
			myaudio.Play();
			hurtSounds[n] = hurtSounds[0];
			hurtSounds[0] = myaudio.clip;
		}
		hitpoints = hitpoints - damage;


	}
	void Attack ()
	{
		if (player != null)
		{
			player.transform.SendMessageUpwards ("Damage",50f, SendMessageOptions.DontRequireReceiver);
		}


	}
	void hitvector(Vector3 davector)
	{
		hitreactionVector = davector;
	}
	void dowander()
	{
		agent.Resume();
		//ray front

		Vector3 wantedvectorfront = transform.forward;
		Ray rayfront = new Ray (transform.position + new Vector3(0f,1f,0f), wantedvectorfront);
		RaycastHit hitfront = new RaycastHit();
		//ray right
		Vector3 wantedvectorright = transform.InverseTransformDirection(transform.right);
		Ray rayright = new Ray (transform.position + new Vector3(0f,1f,0f), wantedvectorright);
		RaycastHit hitright = new RaycastHit();
		//rayleft
		Vector3 wantedvectorleft = transform.InverseTransformDirection(-transform.right);
		Ray rayleft = new Ray (transform.position + new Vector3(0f,1f,0f), wantedvectorleft);
		RaycastHit hitleft = new RaycastHit();

		if (Physics.Raycast(rayfront,out hitfront, walltreshold,mask))
		{
			if (Physics.Raycast(rayright,out hitright, walltreshold,mask))
			{
				Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,-50f,0f);
				transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);
			}
			else
			{
				agent.updateRotation = false;
				Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,50f,0f);
				transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);
			}

			agent.updateRotation = false;
		}
		if (Physics.Raycast(rayfront,out hitfront, walltreshold,mask) && (Physics.Raycast(rayright,out hitright, walltreshold,mask)))
		{

			agent.updateRotation = false;
			Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,-50,0f);
			transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);


		}
		else if (Physics.Raycast(rayfront,out hitfront, walltreshold,mask) && (Physics.Raycast(rayleft,out hitleft, walltreshold,mask)))
		{

			agent.updateRotation = false;
			Vector3 wantedrotation = transform.localEulerAngles + new Vector3(0f,50f,0f);
			transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,wantedrotation, 2f * Time.deltaTime);

		}
		else
		{

			agent.updateRotation = true;
		}





		//animator.SetFloat("Hor", localvelocity.x / walkspeed);
		timer += Time.deltaTime;
		agent.speed = walkspeed;
		if (timer >= wanderTimer) 
		{
			Vector3 newPos = RandomNavSphere(startposition, wanderRadius, -1);
			agent.SetDestination(newPos);
			timer = 0;
		}

	}

}

