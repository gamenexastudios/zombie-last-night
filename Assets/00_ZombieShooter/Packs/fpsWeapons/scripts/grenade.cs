﻿using UnityEngine;
using System.Collections;

public class grenade : MonoBehaviour {
	public Transform explosion;

	public float waitTime = 2.0f;
	public float hitpoints = 50.0f;
	public float radius = 3.0f;
	void Start() {
		StartCoroutine (waitanddestroy());
	}
	void update()
	{
		if (hitpoints <= 0.0f) 
		{
			explode();
		}
	}
	void explode() 
	{
		Instantiate(explosion, transform.position,transform.rotation);
		Vector3 explosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
		foreach (Collider hit in colliders) 
		{
			if(hit.transform.GetComponentInParent<ZombieBehavior>())
			{
//				Debug.Log ("----- Zombie Found");
				if(hit.transform.GetComponentInParent<ZombieBehavior>().mf_Currenthealth>0)
					hit.transform.GetComponentInParent<ZombieBehavior> ().ApplyDamage (100);
			}
		}
		Destroy (gameObject);
	}
	IEnumerator waitanddestroy ()
	{
		yield return new WaitForSeconds (waitTime);
		explode ();
	}
	void Damage (float damage) 
	{
		hitpoints = hitpoints - damage;
	}
	
}