﻿using UnityEngine;
using System.Collections;

public class gunSpring : MonoBehaviour {

	public Transform yrotator;
	public Transform xrotator;
	public float rotateamount = 10f;
	Vector3 wantedRotation;
	Vector3 startRotation;
	public float speed = 1f;
	float curY;
	float curX;
	float yposition;
	float xposition;
	float oldY;
	float oldX;
	float VelocityThreshold = 0.01f;
	float PositionThreshold = 0.01f;
	float y;
	float x;
	float velocityY;
	float velocityX;

	void Start()
	{
		startRotation = transform.localEulerAngles;
	}
	void Update ()
	{



		curY = yrotator.transform.localEulerAngles.y;
		velocityY = Mathf.DeltaAngle (curY, oldY);
		y -= velocityY;		
		y -= yposition * 1.1f;					
		y *= 0.8f;								
		yposition += y * Time.deltaTime * speed;	
		yposition = Mathf.Clamp (yposition, -.3f, .3f);	



		curX = xrotator.transform.localEulerAngles.x;
		velocityX = Mathf.DeltaAngle (curX, oldX);
		x -= velocityX;
		x -= xposition * 1.1f;					
		x *= 0.8f;								
		xposition += x * Time.deltaTime * speed;	
		xposition = Mathf.Clamp (xposition, -.3f, .3f);

		if (Mathf.Abs (y) < VelocityThreshold && Mathf.Abs (yposition) < PositionThreshold) {
			y = 0;
			yposition = 0;
		}
		if (Mathf.Abs (x) < VelocityThreshold && Mathf.Abs (xposition) < PositionThreshold) {
			x = 0;
			xposition = 0;
		}



		wantedRotation = new Vector3 (xposition * rotateamount, yposition * rotateamount, 0f);

		transform.localEulerAngles = startRotation + wantedRotation;

		oldY = curY; 
		oldX = curX; 
	}

}
