﻿using UnityEngine;
using System.Collections;

public class raycastfire : MonoBehaviour 
{
//	public float[] _fWeaponDamages;

	public float force = 500f;
	public float damage = 50f;
	public float range = 100f;
	
	public LayerMask mask;
	public int projectilecount = 1;
	public float inaccuracy = 0.1f;

	public GameObject impactnormal;
	public GameObject impactconcrete;
	public GameObject impactwood;
	public GameObject impactblood;
	public GameObject impactwater;
	public GameObject impactmetal;
	public GameObject impactglass;
	public GameObject impactmelee;
	public GameObject impactnodecal;

	void Start()
	{
//		_fWeaponDamages.Length = weaponselector._instance.Weapons.Length;
//		damage = _fWeaponDamages [weaponselector._instance.currentWeapon];
	}

	public void fireMelee ()
	{
		Vector3 fwrd = transform.forward;
		
		Vector3 camUp = transform.up;
		Vector3 camRight = transform.right;
		
		Vector3 wantedvector = fwrd;
		wantedvector += Random.Range( -.1f, .1f ) * camUp + Random.Range( -.1f, .1f ) * camRight;
		Ray ray = new Ray (transform.position, wantedvector);
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(ray,out hit, 3f,mask))
		{   
			
			if(hit.rigidbody) hit.rigidbody.AddForceAtPosition (500 * fwrd , hit.point);
			hit.transform.SendMessageUpwards ("Damage",50f, SendMessageOptions.DontRequireReceiver);
			GameObject decal;
			if (hit.transform.tag  == "flesh") 
			{
				decal =Instantiate(impactblood, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				//decal.transform.parent = hit.transform;
			}
			else
			{
				decal =Instantiate(impactmelee, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
		}
	}
	// (transform.position, transform.forward);
	RaycastHit Updatehit = new RaycastHit();

	void Update()
	{
		Debug.DrawRay (transform.position, transform.forward * range, Color.cyan);
		Ray ray = new Ray (transform.position, transform.forward);
		if (Physics.Raycast (ray, out Updatehit, range, mask))
		{
			if(Updatehit.transform.tag.Contains("Zombie"))
			{
				if(Updatehit.transform.GetComponentInParent<ZombieBehavior>().mf_Currenthealth>0)
				{
					InGameUIHandler._instance.im_Crosshair.color = Color.red;
					InGameUIHandler._instance.Show_EnemyHealthBar (true,Updatehit.transform.GetComponentInParent<ZombieBehavior>().mf_Currenthealth);
				}
			}
			else if(Updatehit.transform.tag=="Barrel")
			{
				InGameUIHandler._instance.im_Crosshair.color = Color.red;
			}
			else
			{
				InGameUIHandler._instance.im_Crosshair.color = Color.white;
				InGameUIHandler._instance.Show_EnemyHealthBar (false,0);
			}
		}
		else
		{
			InGameUIHandler._instance.im_Crosshair.color = Color.white;
			InGameUIHandler._instance.Show_EnemyHealthBar (false,0);
		}

	}

	public void fire () 
	{
		for(int i = 0; i < projectilecount; i++)
		{
			firebullet();
		}
	}

	void firebullet()
	{
//		Debug.Log ("---- Fired Here");
		GameManager._instance.scr_LevelInfo.Set_AlertEnimies();

		Vector3 fwrd = transform.forward;

		Vector3 camUp = transform.up;
		Vector3 camRight = transform.right;
		
		Vector3 wantedvector = fwrd;
		wantedvector += Random.Range( -inaccuracy, inaccuracy ) * camUp + Random.Range( -inaccuracy, inaccuracy ) * camRight;
		Ray ray = new Ray (transform.position, wantedvector);
		RaycastHit hit = new RaycastHit();
		
		if (Physics.Raycast(ray,out hit, range,mask))
		{   
			
			if(hit.rigidbody) hit.rigidbody.AddForceAtPosition (force * fwrd , hit.point);

			hit.transform.SendMessageUpwards ("Damage",damage, SendMessageOptions.DontRequireReceiver);
			GameObject decal;

			if (hit.transform.tag == "Untagged")  
			{	
				decal =Instantiate(impactnormal, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "concrete") 
			{
				decal =Instantiate(impactconcrete, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "nodecal") 
			{
				decal =Instantiate(impactnodecal, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "metal") 
				
			{
				decal =Instantiate(impactmetal, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "wood") 
			{
				decal =Instantiate(impactwood, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "water") 
			{
				decal =Instantiate(impactwater, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "glass") 
			{
				decal =Instantiate(impactglass, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				decal.transform.parent = hit.transform;
			}
			else if (hit.transform.tag  == "flesh") 
			{
				hit.transform.SendMessageUpwards ("hitvector",hit.point, SendMessageOptions.DontRequireReceiver);
				decal =Instantiate(impactblood, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
			}

			else if(hit.transform.tag  == "Zombie")
			{
				decal =Instantiate(impactblood, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				if(hit.transform.GetComponentInParent<ZombieBehavior> ().mf_Currenthealth>0)
					hit.transform.GetComponentInParent<ZombieBehavior> ().ApplyDamage (damage,5,ray,hit);
			}
			else if(hit.transform.tag  == "Zombie_Head")
			{
				decal =Instantiate(impactblood, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				if(hit.transform.GetComponentInParent<ZombieBehavior> ().mf_Currenthealth>0)
					hit.transform.GetComponentInParent<ZombieBehavior> ().ApplyDamage (100,0,ray,hit);
			}
			else if(hit.transform.tag  == "Special_Zombie")
			{
				decal =Instantiate(impactblood, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject ;
				decal.transform.localRotation = decal.transform.localRotation * Quaternion.Euler(0,Random.Range(-90,90),0);
				if(hit.transform.GetComponentInParent<ZombieBehavior> ().mf_Currenthealth>0)
					hit.transform.GetComponentInParent<ZombieBehavior> ().ApplyDamage (damage/2,0,ray,hit);
			}
		}
	}
}
