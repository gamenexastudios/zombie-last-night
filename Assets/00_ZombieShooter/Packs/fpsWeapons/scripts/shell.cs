﻿using UnityEngine;
using System.Collections;

public class shell : MonoBehaviour {
	public float  waitTime = 2f;
    AudioSource myAudioSource;
	public AudioClip[] shellsounds ;
	public LayerMask desiredmask;
    // Use this for initialization

    private void OnEnable()
    {
        myAudioSource = GameManager._instance.shellSounds;
    }
    void Start () {
		Destroy (gameObject, waitTime);	
		transform.localRotation = transform.localRotation * Quaternion.Euler(0,Random.Range(-90f,90f),0);
	}
	

	void OnCollisionEnter( Collision collision) 
	{
		if (gameObject.layer != desiredmask.value) 
		{
			gameObject.layer = 1 << desiredmask.value;
		}

			AudioClip clips = shellsounds[Random.Range(0,shellsounds.Length)];
            myAudioSource.PlayOneShot(clips);
		
	}

}
