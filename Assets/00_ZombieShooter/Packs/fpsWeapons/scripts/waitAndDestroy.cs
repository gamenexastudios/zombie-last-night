﻿using UnityEngine;
using System.Collections;

public class waitAndDestroy : MonoBehaviour {
	public float waitTime = 5f;
    // Use this for initialization

    AudioSource audio;
    public bool bholeStone;
    private void OnEnable()
    {
        if (bholeStone)
        {
            audio = GameManager._instance.bholestoneAudio;
            audio.Play();
        }
        else
        {
            audio = GameManager._instance.BloodSplash;
            audio.Play();
        }

    }
    void Start () 
	{
		Destroy (gameObject, waitTime);
	
	}

}
