﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class weaponselector : MonoBehaviour {
	public int currentWeapon = 0;
	//public int numWeapons = 0;
	public static weaponselector _instance;
	public Transform[] Weapons;
	public bool[] HaveWeapons;
	public int[] WeaponsAmmo;
	public int grenade;
	public float lastGrenade;
	public float selectInterval = 2f;
	private float nextselect = 2f;

	private int previousWeapon = 0;
	public  AudioClip switchsound;
	public AudioSource myaudioSource;
	public bool canswitch;

	public bool hideweapons = false;
	bool oldhideweapons = false;
	public Text ammotext;
	public Text grenadetext;
	public int currentammo = 10;
	//public int totalammo = 100;

	public GameObject AIM;

	int oldAmmo=-1;
	int oldTotalAmmo=-1;
	public int _iShortGun;
	void Awake()
	{
		_instance = this;

		for(int i = 0; i < Weapons.Length; i++)
		{
			Weapons[i].gameObject.SetActive(false);
		}

//		StaticVariables._iSelectedWeapon = StaticVariables._iSelectedWeapon;

		// Manually Selected Guns For Testing Purpose Only
//		if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Career)
//		{
//			if(StaticVariables._iCurrentLevel<5)
//				StaticVariables._iSelectedWeapon = 0;
//			else if(StaticVariables._iCurrentLevel>=5 && StaticVariables._iCurrentLevel<9)
//				StaticVariables._iSelectedWeapon = 2;
//			else if(StaticVariables._iCurrentLevel>=9 && StaticVariables._iCurrentLevel<12)
//				StaticVariables._iSelectedWeapon = 3;
//			else if(StaticVariables._iCurrentLevel>=12 && StaticVariables._iCurrentLevel<15)
//				StaticVariables._iSelectedWeapon = 4;
//			else if(StaticVariables._iCurrentLevel>=15 && StaticVariables._iCurrentLevel<18)
//				StaticVariables._iSelectedWeapon = 5;
//			else if(StaticVariables._iCurrentLevel>=18 && StaticVariables._iCurrentLevel<20)
//				StaticVariables._iSelectedWeapon = 7;
//			else if(StaticVariables._iCurrentLevel>=20)
//				StaticVariables._iSelectedWeapon = 8;
//		}

		if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Bounty || StaticVariables.m_GameMode==StaticVariables.e_GameMode.Survival)
		{
			currentWeapon = 5;
//			StaticVariables._iSelectedWeapon = 5;
		}
		else
		{
			currentWeapon = StaticVariables._iSelectedWeapon - 1;
		}
//		currentWeapon=StaticVariables._iSelectedWeapon-1;
		canswitch = true;
		HaveWeapons=new bool[Weapons.Length];
		HaveWeapons[0]=true; // Pistol
		if(WeaponsAmmo.Length==0) WeaponsAmmo=new int[Weapons.Length];

		//for test ONLY, to enable all weapons

		for (int i=0;i<Weapons.Length;i++){
			HaveWeapons[i]=true;
		}
	}
	void Start () 
	{
		StartCoroutine (selectWeapon (currentWeapon));
		grenade=StaticVariables._iGranedeCount;
	}

	void Update () 
	{
		bool changeAmmoText=false;
		if(oldAmmo!=currentammo){
			oldAmmo=currentammo;
			changeAmmoText=true;
		}
		if(oldTotalAmmo!=WeaponsAmmo[currentWeapon]){
			oldTotalAmmo=currentammo;
			changeAmmoText=true;
		}
		if(changeAmmoText){
			if(WeaponsAmmo[currentWeapon]==-1)
				ammotext.text="";
			else
				ammotext.text = (currentammo + " / " + WeaponsAmmo[currentWeapon]);
		}
		grenadetext.text = grenade.ToString();

		/*if(Input.GetAxis("CycleWeapons")>0 && Time.time > nextselect && canswitch 
		   || 
		   (Input.GetButtonDown ("CycleWeapons") && Time.time > nextselect && canswitch && !hideweapons))

		{
			nextselect = Time.time + selectInterval;

			int weaponOK=0;

			for(int i=currentWeapon +1 ; i<Weapons.Length;i++){
				if(HaveWeapons[i]){
					weaponOK=i;
					break;
				}
			}

			previousWeapon = currentWeapon;
			currentWeapon=weaponOK;

			if(currentWeapon!=previousWeapon){
				//Debug.Log("Subtracted");
				playSwithSound();
				StartCoroutine(selectWeapon(currentWeapon));
			}


			// ================Previous Weapon========================
		}
		else if(Input.GetAxis("CycleWeapons")<0 && Time.time > nextselect && canswitch && !hideweapons)
		{
			nextselect = Time.time + selectInterval;

			int weaponOK=0;

			if(currentWeapon>0){
				for(int i=currentWeapon-1 ; i>-1; i--){
					if(HaveWeapons[i]){
						weaponOK=i;
						break;
					}
				}
			}else{
				for(int i=Weapons.Length-1 ; i>-1; i--){
					if(HaveWeapons[i]){
						weaponOK=i;
						break;
					}
				}
			}

			previousWeapon = currentWeapon;
			currentWeapon=weaponOK;
			if(currentWeapon!=previousWeapon){
				//Debug.Log("Added");
				playSwithSound();
				StartCoroutine(selectWeapon(currentWeapon));
			}
		}*/
		if (hideweapons!= oldhideweapons)
		{
			if(hideweapons)
			{
				StartCoroutine(hidecurrentWeapon(currentWeapon));
			}
			else
			{
				StartCoroutine(unhidecurrentWeapon(currentWeapon));
			}
		}
	}

	public void playSwithSound(){
		myaudioSource.PlayOneShot(switchsound, 1);
	}

	public void PickAmmo(int weaponNumber,int amountAmmo){
		WeaponsAmmo[weaponNumber]+=amountAmmo;
		if(weaponNumber==currentWeapon)
			Weapons[weaponNumber].gameObject.BroadcastMessage("pickAmmo",WeaponsAmmo[weaponNumber]);
	}

	public void InitCurrentWeaponAmmo(int amountAmmo){
		if(WeaponsAmmo.Length==0)WeaponsAmmo=new int[Weapons.Length];
		//Debug.Log("currentWeapon="+currentWeapon);
		WeaponsAmmo[currentWeapon]+=amountAmmo;
	}

	public void UpdateCurrentWeaponAmmo(int amountAmmo){
		WeaponsAmmo[currentWeapon]=amountAmmo;
	}

	IEnumerator hidecurrentWeapon(int index)
	{
		Weapons[index].gameObject.BroadcastMessage("doRetract",SendMessageOptions.DontRequireReceiver);
		yield return new WaitForSeconds (0.15f);
		Weapons[index].gameObject.SetActive(false);
		oldhideweapons = hideweapons;
	}

	IEnumerator unhidecurrentWeapon(int index)
	{
		yield return new WaitForSeconds (0.15f);
		Weapons[index].gameObject.SetActive(true);
		Weapons[index].gameObject.BroadcastMessage("doNormal",SendMessageOptions.DontRequireReceiver);
		oldhideweapons = hideweapons;
	}

	IEnumerator selectWeapon(int index)
	{
		Weapons[previousWeapon].gameObject.BroadcastMessage("doRetract",SendMessageOptions.DontRequireReceiver);
		yield return new WaitForSeconds (0.5f);
		Weapons[previousWeapon].gameObject.SetActive(false);
		for (int i = 0; i < Weapons.Length; i++)
			Weapons [i].gameObject.SetActive (false);
		Weapons[index].gameObject.SetActive(true);
		Weapons[index].gameObject.BroadcastMessage("doNormal",SendMessageOptions.DontRequireReceiver);
	}

	public void showAIM(bool show)
	{
		if(show)
			AIM.SetActive(true);
		else
			AIM.SetActive(false);
	}
	public GameObject mg_TorchObj;

	public void Call_ShowTorchWeapon()
	{
		for(int i=0;i<Weapons.Length;i++)
		{
			Weapons [i].gameObject.SetActive (false);
		}
		mg_TorchObj.SetActive (true);
	}
	public void Call_HideTorchWeapon()
	{
		for(int i=0;i<Weapons.Length;i++)
		{
			Weapons [i].gameObject.SetActive (false);
		}
		mg_TorchObj.SetActive (false);

		StartCoroutine (selectWeapon (currentWeapon));

	}
}
