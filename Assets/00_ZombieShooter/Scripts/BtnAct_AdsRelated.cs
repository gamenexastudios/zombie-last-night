﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySystem;

public class BtnAct_AdsRelated : MonoBehaviour
{
    public static BtnAct_AdsRelated myScript;

    //static bool Is_Created;
    public void Awake()
    {
        //if (!Is_Created)
        //{
            //DontDestroyOnLoad(gameObject);
            myScript = this;
        //Is_Created = true;
        //}
        //else
        //{
        //Destroy(gameObject);
        //}

        //************************************************************GAME NEXA CODE************************************************************
    }

    public void BtnAct_CallRewardVideo(int _videoType)
    {
        #region Commented
        //        if (!MyAdsManager.myScript)
        //            return;

        //        if (MyAdsReadXML.Is_TurnOffAllAds)
        //            return;


        //        MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.None;
        //        switch (_videoType)
        //        {
        //            case 0:
        //                MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.FreeCoins;
        //                break;
        //            case 1:
        //                MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.Upgrade;
        //                break;
        //            case 2:
        //                MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.GameResume;
        //                break;
        //            case 3:
        //                MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.DoubleReward;
        //                break;
        //            case 4:
        //                MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.DailyBonusDouble;
        //                break;
        //            case 5:
        //                MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.Welcome2x;
        //                break;
        //        }
        //        MyAdsManager.myScript.ShowRewardVideo();

        //#if UNITY_EDITOR
        //        if (MyAdsManager.myScript)
        //            MyAdsManager.myScript.Invoke("RewardVideoSuccess", 1);
        //#endif
        #endregion

        //************************************************************GAME NEXA CODE************************************************************
#if !AdSetup_OFF
        GoogleAdMobsManager.instance._rewardType = rewardType.coins;
        GoogleAdMobsManager.instance.DisplayRewardVideo(500);
#endif

        //Requesting and keeping it ready for next time we need to display!!
        //GoogleAdMobsManager.instance.RequestRewardVideo();
    }

    public void BtnAct_CallUnityRewardVideo()
    {
        //AdsInit_UnityVideo.myScript.ShowRewardedVideo();
    }

    public void BtnAct_CallIAPBuy(int _index)
    {
        //IAPManager.myScript.BuyProductID(_index);
#if !AdSetup_OFF
        IAPManager._instance.BuyProductID(_index);
#endif
    }

    public static void CallIAPBuy(int _index)
    {
        //IAPManager.myScript.BuyProductID(_index);
    }

    public void BtnAct_MoreGames()
    {
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/dev?id=6143935442918635295&hl=en");
#elif UNITY_IOS
        Application.OpenURL("");
#endif
    }

    public void Call_RateGame()
    {
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
#elif UNITY_IOS
        Application.OpenURL("");
#endif
    }

    public void Call_FBShareGame()
    {
       print("Call_FBShareGame");
    }

    public void Call_NativeShare()
    {
        print("Call_NativeShare Here");
    }
    public void Call_NativeShareGameScore()
    {

    }
    public void BtnAct_SignIn()
    {
        
    }

    public void BtnAct_LB()
    {
      
    }

    public void BtnAct_Ach()
    {
       
    }

    public void BtnAct_Policy()
    {
        Application.OpenURL("http://gamenexastudio.blogspot.com/2017/10/privacy-policy.html");
    }


    public void BtnAct_Terms()
    {
        Application.OpenURL("http://gamenexastudio.blogspot.com/2017/10/term-of-use.html");
    }
}
