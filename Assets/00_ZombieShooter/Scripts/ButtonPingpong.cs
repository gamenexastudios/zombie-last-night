﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPingpong : MonoBehaviour {

	// Use this for initialization
	public float mf_EffectAmount=1f;
	public float mf_DelaytoStart=1.5f;
	public float mf_Time=0.25f;
	void Start () 
	{
		StopCoroutine("ButtonEffect");
		StartCoroutine (ButtonEffect (1));
	}
	IEnumerator ButtonEffect(float delay)
	{
		yield return new WaitForSeconds (delay);
		iTween.PunchRotation (gameObject, iTween.Hash ("z", mf_EffectAmount, "time", mf_Time, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
	}
}
