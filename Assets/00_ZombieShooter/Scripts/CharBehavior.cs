﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharBehavior : MonoBehaviour 
{
	public Animator m_Animator;
	public float _fMaxHealth=100, _fCurrentHealth;
	public Image im_HealthBar;
	void Start () 
	{
		m_Animator = GetComponent<Animator> ();
		_fCurrentHealth = _fMaxHealth;
	}

	public void ApplyDamagetoOthers(int _val)
	{
		_fCurrentHealth -= _val;
		im_HealthBar.fillAmount = _fCurrentHealth / _fMaxHealth;
        Debug.Log("Attacking Other Char : "+ _fCurrentHealth);

        if (_fCurrentHealth<=0)
		{
			_fCurrentHealth = 0;
			m_Animator.SetInteger ("AnimCount", 2);
			if(GameManager._instance.m_GameState!=GameManager.e_GameState.LF)
			{
				GameManager._instance.m_GameState = GameManager.e_GameState.LF;
				Debug.Log ("---- LF :: You have failed to Save Others ");
				InGameUIHandler._instance.Invoke("Call_LF",2f);
			}
		}
		else{
//			m_Animator.SetInteger ("AnimCount", 1);
//			transform.LookAt
		}
	}
}
