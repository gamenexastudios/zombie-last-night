﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsOpen : MonoBehaviour 
{
	public Rigidbody[] _rb;

	bool _isOpened;
	public float _fForceVal=500;
	void OnTriggerEnter(Collider _obj)
	{
		if(_obj.transform.tag.Contains("Zombie"))
		{
			if(!_isOpened)
			{
				_isOpened = true;
				BlastDoors ();
			}
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.B))
			BlastDoors ();
	}

	void BlastDoors()
	{
//		Time.timeScale = 0.5f;
//		Invoke ("SetTime", 1.2f);
		for(int i=0;i<_rb.Length;i++)
		{
			_rb [i].useGravity = true;
			_rb [i].isKinematic = false;
			_rb [i].AddExplosionForce (Random.Range(_fForceVal,_fForceVal+50),transform.position,5,2f);
		}
	}
	void SetTime()
	{
		Time.timeScale = 1f;
	}
}
