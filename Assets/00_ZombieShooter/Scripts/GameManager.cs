﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MySystem;

public class GameManager : MonoBehaviour 
{
	public static GameManager _instance;

	public enum e_GameState
	{
		None,
		GamePlay,
		Pause,
		Revive,
		LC,
		LF
	}
	public e_GameState m_GameState;

	public enum e_WeaponType
	{
		None,
		Assault,
		DoublePistol,
		Sniper,
		ShotGun,
		Flamer,
		MachineGun,
		RPG,
		Slugger
	}
	public e_WeaponType m_WeaponType;

	public GameObject scr_Shooter;
	public GameObject mg_PlayerIns,mg_FinishCam;
	public LevelInfo scr_LevelInfo;
	public GameObject mg_Envi;

//	public List<GameObject> mlist_ZombiePrefabs;
//	public List<GameObject> mlist_SplZombiePrefabs;

	[Header("============= Career Mode REGION =================")]
	public List <LevelInfo> mlist_Levels;
	public List<Transform> mlistPos;
	public int  mi_CurrentLevel;

	[Header("============= Bounty Mode REGION =================")]
	public List<LevelInfo> mlist_BountyLevels;
	public int _iBountyLevel;



    [Header("============= AudioSources =================")]
    public AudioSource bholestoneAudio;
    public AudioSource shellSounds;
    public AudioSource BloodSplash;
    public AudioSource ZombieAudio;





    [Header("============= TESTING REGION =================")]
	public bool _isTest;
	public int _iTestLevel;




	void Awake()
	{
		_instance = this;
//		StaticVariables.m_GameMode = StaticVariables.e_GameMode.Bounty;
		if(mg_Envi)
			mg_Envi.SetActive (true);

		mi_CurrentLevel = StaticVariables._iCurrentLevel;

//		mg_PlayerIns = Instantiate (Resources.Load ("FirstPersonController New"), mlistPos [StaticVariables._iCurrentLevel - 1].position, mlistPos [StaticVariables._iCurrentLevel - 1].rotation)as GameObject;
		MapCanvasController.Instance.playerTransform = Camera.main.transform;
	}

	void Start ()
	{
		m_GameState = e_GameState.GamePlay;
		if (StaticVariables.m_GameMode == StaticVariables.e_GameMode.Career || StaticVariables.m_GameMode == StaticVariables.e_GameMode.Bounty)
		{
			Set_InitialProperties ();
		} 
		else if (StaticVariables.m_GameMode == StaticVariables.e_GameMode.Survival) 
		{
			mg_PlayerIns.transform.position = SurvivalMode._instance.gameObject.transform.position;
			mg_PlayerIns.transform.rotation = SurvivalMode._instance.gameObject.transform.rotation;
			SurvivalMode._instance.Set_SurvivalModeProperties (mg_PlayerIns.transform.position);
		}
		AudioListener.volume = StaticVariables.iSoundStatus;
		if(SoundsHandler._instance)
		{
			SoundsHandler._instance.StopAllSounds ();
			if(SceneManager.GetActiveScene().name=="Theme_1") {
				Debug.Log ("---- Theme1 SOUND");
				SoundsHandler._instance.Sound_Theme1.Play ();
			}
			else if(SceneManager.GetActiveScene().name=="Theme_2AA")
				SoundsHandler._instance.Sound_Theme2.Play ();
			else if(SceneManager.GetActiveScene().name=="Theme_3BNew")
				SoundsHandler._instance.Sound_Theme3.Play ();
			else if(SceneManager.GetActiveScene().name=="BountySet_2_AfterBaking")
				SoundsHandler._instance.Sound_Theme1.Play ();
		}

		Resources.UnloadUnusedAssets();
		System.GC.Collect();
    }

	void Set_InitialProperties()
	{
		#if UNITY_EDITOR
		if (_isTest)
		{
			StaticVariables._iCurrentLevel = _iTestLevel;
		}
		#endif

		if (StaticVariables.m_GameMode == StaticVariables.e_GameMode.Career)
		{
			MyDebug.Log ("<color=magenta> Current Level :::: </color>"+StaticVariables._iCurrentLevel +" && "+ "<color=blue> Current Mode :::: </color>"+StaticVariables.m_GameMode);

			mg_PlayerIns.transform.position = mlistPos [StaticVariables._iCurrentLevel - 1].position;
			mg_PlayerIns.transform.rotation = mlistPos [StaticVariables._iCurrentLevel - 1].rotation;

			foreach (LevelInfo _level in mlist_Levels)
				_level.gameObject.SetActive (false);

			mlist_Levels [StaticVariables._iCurrentLevel - 1].gameObject.SetActive (true);
			scr_LevelInfo = mlist_Levels [StaticVariables._iCurrentLevel - 1].GetComponent<LevelInfo> ();
		}
		else if (StaticVariables.m_GameMode == StaticVariables.e_GameMode.Bounty)
		{
//			StaticVariables._iSelectedWeapon = 5;
			int _rand = Random.Range (0, mlist_BountyLevels.Count);
			_iBountyLevel = _rand;
			MyDebug.Log ("<color=magenta> Current Level :::: </color>"+_rand +" && "+ "<color=blue> Current Mode :::: </color>"+StaticVariables.m_GameMode);

			StaticVariables._iCurrentLevel = _rand;

			mlist_BountyLevels [_rand].gameObject.SetActive (true);
			scr_LevelInfo = mlist_BountyLevels [_rand].GetComponent<LevelInfo> ();
		}
	}

	public void Call_ResumeGamePlay()
	{
		m_GameState = e_GameState.GamePlay;
		InGameUIHandler._instance.Revive.gameObject.SetActive (false);
		InGameUIHandler._instance.Revive.Call_TweenOut ();
		playercontroller._instance.mf_PlayerCurrentHealth = 75;
		InGameUIHandler._instance.Show_PlayerHealthBar (75);
	}

	public void Enable_FinalEffect(Vector3 pos)
	{
		mg_FinishCam.SetActive(true);
		mg_FinishCam.transform.rotation = Camera.main.transform.rotation;
//		if(SceneManager.GetActiveScene().name=="GamePlay_1")
//			mg_FinishCam.transform.position = pos + new Vector3 (5, 2, 10);
//		else
			mg_FinishCam.transform.position = pos + new Vector3 (0, 2, 0);

		Vector3 _pos = mg_FinishCam.transform.position + new Vector3 (0, 0, -2);
		iTween.MoveTo (mg_FinishCam.gameObject, iTween.Hash ("position", _pos, "Time", 1f, "easetype", iTween.EaseType.linear));
		Time.timeScale = 0.5f;
		Invoke ("Disable_FinalEffect", 2f);
	}

	void Disable_FinalEffect()
	{
		Time.timeScale = 1f;
		mg_FinishCam.SetActive(false);
	}

	public void Call_LevelFailEffect()
	{
		
	}
}
