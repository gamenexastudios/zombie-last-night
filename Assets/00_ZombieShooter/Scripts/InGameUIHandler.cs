﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MySystem;

public class InGameUIHandler : MonoBehaviour 
{
	public static InGameUIHandler _instance;

	void Awake()
	{
		_instance = this;
	}
	[Header("=========== PlayArea ============")]
	public GameObject _gCareerUI;
	public GameObject _gSurvivalUI;
	public Image im_Crosshair;
	public Image im_EnemyHealthBar;
	public Image im_PlayerHeathBar;
	public Image im_SlowMo;
	public Text Text_ShotName,Text_Targets;
	public Text Text_GranedeCount,Text_HealthCount,Text_SlowMoCount;
	public GameObject mg_Pause;
	public PageAnimHandler LevelComplete,BountyLC;
	public PageAnimHandler LevelFail,BountyLF,SurvivalResults,Revive;
	public GameObject mg_WarningObj;
	public Text Text_Warn;
	public Image im_Arrow;
	public Image im_Headshot;
	[HideInInspector]public bool mb_Zoom,mb_Torch;
	public GameObject mg_FadeObj,mg_BloodObj;
    public GameObject[] BloodEffects;
	[Header("=========== LC REGION ============")]
	public GameObject mg_Btn2X;
	public Text Text_ZombiesKilled;
	public Text Text_HeadShotReward,Text_LevelReward,Text_TotalReward,Text_LevelGold;
	public int _iHeadShots=0;
	public Image im_LCLevel,im_LFLevel;
	public Sprite[] spr_Levels;
	[Header("=========== Survival REGION ============")]
	public Text Text_SurvivalKilledCount;
	public Text Text_SurvivalHeadShotReward,Text_SurvivalTotalReward,Text_SurvivalGold;
	public Text Text_KilledCount;
	[Header("=========== BOUNTY REGION ============")]
	public Text Text_BountyKilledCount;
	public Text Text_BountyHeadShotReward,Text_BountyTotalReward;
	public Text Text_BountyGold;
	[Header("=========== EXTRA ============")]
	public weaponselector inventory;
	public GameObject mg_Help;
	public GameObject[] mg_BtnUnlockAllLvls, mg_BtnUnlockAllUpgrades;

	public List<GameObject> mlist_ZombiePrefabs;
	public List<GameObject> mlist_SplZombiePrefabs;


    public int PauseButtonAd;
    public int HomeButtonAd;

    //Game Nexa code
    public GameObject noGrenadePannel;
	void Check_BtnVisibility()
	{
		if (StaticVariables.m_iUnlockedLevels >= 30) {
			for(int i=0;i<mg_BtnUnlockAllLvls.Length;i++)
			mg_BtnUnlockAllLvls[i].SetActive (false);
		}
		if (StaticVariables.strUnlockedWeapons == StaticVariables.strUnlockedWeaponsRef)
			for(int i=0;i<mg_BtnUnlockAllUpgrades.Length;i++)
			mg_BtnUnlockAllUpgrades[i].SetActive (false);
	}

	void Start()
	{
        //if (FirebaseRemoteConfiguration.instance)
        //{
        //    PauseButtonAd = int.Parse(FirebaseRemoteConfiguration.instance.data.PlayButtonAd);
         //   HomeButtonAd = int.Parse(FirebaseRemoteConfiguration.instance.data.HomeButtonAd);
        //}
       // else
       // {
            PauseButtonAd = 0;
            HomeButtonAd = 0;
       // }


		if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Career)
		{
			_gCareerUI.SetActive (true);
			_gSurvivalUI.SetActive (false);
			if(StaticVariables._iCurrentLevel==1 && StaticVariables._iHelpStatus==0)
			{
				mg_Help.SetActive (true);
			}
		}
		else if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Survival)
		{
			_gCareerUI.SetActive (false);
			_gSurvivalUI.SetActive (true);
		}
//		if()

		Update_GranedeCount ();Update_HealthCount ();Update_SlowMoCount ();
		Check_InfoWarn ();
		Check_BtnVisibility ();


        Show_PlayerHealthBar(100);

    }
	void Check_InfoWarn()
	{
		if(StaticVariables._iCurrentLevel==3 || StaticVariables._iCurrentLevel==5) 
		{ 
			mg_WarningObj.SetActive (true);
			Text_Warn.text = "Be Alert...\nPowerful Zombie Attacks will be there";
			Invoke ("Disable_Warn", 3f);
		}
		else
			if(StaticVariables._iCurrentLevel==4)
			{
				mg_WarningObj.SetActive (true);
				Text_Warn.text = "Take Down Zombies Before they Attacking Others";
				Invoke ("Disable_Warn", 3f);
			}
	}
	void Disable_Warn()
	{
		mg_WarningObj.SetActive (false);
	}

	public void OnBtnClick(string _str)
	{
		if(SoundsHandler._instance)
		{
			SoundsHandler._instance.Sound_BtnClick.Play ();
		}
		switch(_str)
		{
		case "Btn_Pause":
#if !AdSetup_OFF
                if(PauseButtonAd == 1)
                GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif
                // GoogleAdMobsManager.instance.RequestInterstitialAd();
                Call_Pause (true);
			break;
		case "Btn_Resume":
			Call_Pause (false);
                noGrenadePannel.SetActive(false);
                break;
		case "Btn_PauseRetry":
			Call_Pause (false);
			if (GameManager._instance.m_GameState != GameManager.e_GameState.LF || GameManager._instance.m_GameState != GameManager.e_GameState.LC) 
			{
				GameManager._instance.m_GameState = GameManager.e_GameState.LF;
                    SceneManager.LoadScene("LoadingScene");
                   // Call_LF ();
			}

			break;
		case "Btn_Zoom":
			mb_Zoom = !mb_Zoom;
			break;
		case "Btn_Granede":
                if (StaticVariables._iGranedeCount > 0)   //Game nexa code
                {
                    if (inventory.grenade > 0)
                    {
                        StaticVariables._iGranedeCount -= 1;
                        if (Time.timeSinceLevelLoad > (inventory.lastGrenade + 1))
                        {
                            inventory.lastGrenade = Time.timeSinceLevelLoad;
                            if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Assault)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<genericShooter>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.DoublePistol)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<akimboShooter>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Sniper)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<sniper>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.ShotGun)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<shotgun>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Flamer)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<flamethrower>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.MachineGun)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<minigun>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.RPG)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<RPG>().setThrowGrenade());
                            else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Slugger)
                                StartCoroutine(GameManager._instance.scr_Shooter.GetComponent<slugger>().setThrowGrenade());
                        }
                        Update_GranedeCount();
                        Debug.Log("---- We Can Throw Granede");
                    }
                }

                //else
                //{
                //    Call_Pause(true);
                //    noGrenadePannel.SetActive(true);
                //}

			break;
		case "Btn_Flash":
			mb_Torch = !mb_Torch;
			if(mb_Torch)
				weaponselector._instance.Call_ShowTorchWeapon ();
			else
				weaponselector._instance.Call_HideTorchWeapon ();
			break;
		case "Btn_Health":
			if(StaticVariables._iHealthCount>0)
			{
                    if (inventory.GetComponent<playercontroller>().mf_PlayerCurrentHealth < 100f)
                    {
                        StaticVariables._iHealthCount -= 1;
                        Update_HealthCount();
                        Debug.Log("---- Give Heath +50% ");
                        inventory.GetComponent<playercontroller>().mf_PlayerCurrentHealth += 50f;
                        if (inventory.GetComponent<playercontroller>().mf_PlayerCurrentHealth > 100)
                            inventory.GetComponent<playercontroller>().mf_PlayerCurrentHealth = 100;
                        InGameUIHandler._instance.Show_PlayerHealthBar(inventory.GetComponent<playercontroller>().mf_PlayerCurrentHealth);
                    }
                }

			else
			{
				Debug.Log ("---- Health Count is Zero");
			}
			break;
		case "Btn_SlowMotion":
			if(StaticVariables._iSlowMotionCount>0)
			{
				Debug.Log ("---- SlowMotionActivated");
				StaticVariables._iSlowMotionCount -= 1;
				Update_SlowMoCount ();
				im_SlowMo.enabled = true;
				GameManager._instance.scr_LevelInfo.Enable_SlowMotion ();
				Invoke ("SlowMotionEnding", 3.5f);
			}
			else
			{
				Debug.Log ("---- SlowMotion Count is Zero");
			}
			break;
			#region LC
		case "Btn_Home":
                Debug.LogWarning("Home Button Clicked");
			Time.timeScale=1;
			StaticVariables._iEnablePageID = 0;
			StaticVariables._sSceneToLoad="Menu";
                //  SceneManager.LoadScene ("LoadingScene");
                //Changes
                SceneManager.LoadScene("Menu");
                ServicesStaticVariables._homeButtonPressed = true;
                //End Changes     
                break;

            case "Btn_PausetoHome":
                #if !AdSetup_OFF
                if (HomeButtonAd == 1)
                    GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif
                Time.timeScale = 1;
                StaticVariables._iEnablePageID = 0;
                StaticVariables._sSceneToLoad = "Menu";

                SceneManager.LoadScene("LoadingScene");
                break;

            case "Btn_LCNext":
			StaticVariables._iEnablePageID = 5;
			StaticVariables._sSceneToLoad="Menu";
                //SceneManager.LoadScene("LoadingScene");
                //Changes
                SceneManager.LoadScene("Menu");
                ServicesStaticVariables._retryButtonPressed = true;
                break;
		case "Btn_2X":
                if (BtnAct_AdsRelated.myScript)
                {
                    BtnAct_AdsRelated.myScript.BtnAct_CallRewardVideo(3);
                }
			break;
		case "Btn_SurvivalNext":
			StaticVariables._iEnablePageID = 4;
			StaticVariables._sSceneToLoad="Menu";
			SceneManager.LoadScene ("LoadingScene");
			break;
#endregion

		case "Btn_HelpContinue":
			mg_Help.SetActive (false);
			StaticVariables._iHelpStatus = 1;
			break;

            case "Btn_RetryGames":
                //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                SceneManager.LoadScene("LoadingScene");
                break;
		}
	}
	void Update_GranedeCount()
	{
		Text_GranedeCount.text = StaticVariables._iGranedeCount.ToString ();
	}
	void Update_HealthCount()
	{
		Text_HealthCount.text = StaticVariables._iHealthCount.ToString ();
	}
	void Update_SlowMoCount()
	{
		Text_SlowMoCount.text = StaticVariables._iSlowMotionCount.ToString ();
	}
	public void Btn_Shoot (bool _isFire) 
	{
		if (!mb_Torch)
		{
			if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Assault)
				GameManager._instance.scr_Shooter.GetComponent<genericShooter> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.DoublePistol)
				GameManager._instance.scr_Shooter.GetComponent<akimboShooter> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Sniper)
				GameManager._instance.scr_Shooter.GetComponent<sniper> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.ShotGun)
				GameManager._instance.scr_Shooter.GetComponent<shotgun> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Flamer)
				GameManager._instance.scr_Shooter.GetComponent<flamethrower> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.MachineGun)
				GameManager._instance.scr_Shooter.GetComponent<minigun> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.RPG)
				GameManager._instance.scr_Shooter.GetComponent<RPG> ()._canShoot = _isFire;
			else if (GameManager._instance.m_WeaponType == GameManager.e_WeaponType.Slugger)
				GameManager._instance.scr_Shooter.GetComponent<slugger> ()._canShoot = _isFire;
		} 
		else
			flashlight._instance._canShoot = _isFire;
	}
	public void Update_TargetText(int _val=0)
	{
		Text_Targets.text =_val+ " / " + GameManager._instance.scr_LevelInfo.mi_Totaltargets;
		if(_val==GameManager._instance.scr_LevelInfo.mi_Totaltargets)
		{
			if(GameManager._instance.m_GameState!=GameManager.e_GameState.LC)
			{
				GameManager._instance.m_GameState = GameManager.e_GameState.LC;
				Debug.Log ("---- LC Count Based");
				InGameUIHandler._instance.Invoke("Call_LC",2f);
			}
		}
	}
	public void Show_EnemyHealthBar(bool _show=false,float _val=0)
	{
		im_EnemyHealthBar.transform.parent.gameObject.SetActive (_show);
		im_EnemyHealthBar.fillAmount = _val / 100;
	}

	public void Show_PlayerHealthBar(float _val=0)
	{
        im_PlayerHeathBar.fillAmount = _val / 100;

        if (im_PlayerHeathBar.fillAmount >=0.8f)
        im_PlayerHeathBar.color = Color.green;

        if (im_PlayerHeathBar.fillAmount > 0.5f && im_PlayerHeathBar.fillAmount <= 0.8f)
            im_PlayerHeathBar.color = Color.yellow;

        if (im_PlayerHeathBar.fillAmount < 0.5f)
            im_PlayerHeathBar.color = Color.red;

        if(im_PlayerHeathBar.fillAmount <= 0.9f)
        StartCoroutine(mg_BloodObj.GetComponent<FadeInOut>().Call_Fade(0.01f));

    }
	public void Show_ShotName(int _shot)
	{
		if(_shot==0) {
			im_Headshot.gameObject.SetActive (true);
//			Text_ShotName.text = "Head Shot \n +100";
		}
		_iHeadShots += 1;
		iTween.ScaleFrom (im_Headshot.gameObject, iTween.Hash ("Scale", Vector3.zero, "time", 0.25f, "Easetype", iTween.EaseType.easeOutBounce));
		iTween.MoveFrom (im_Headshot.gameObject, iTween.Hash ("y",100,"time", 0.35f,"islocal",true, "easetype", iTween.EaseType.linear));
		Invoke ("ResetText", 0.4f);
	}

	void ResetText()
	{
		im_Headshot.gameObject.SetActive (false);

//		Text_ShotName.text = " ";
	}
#region GameStates


	int _iLevelReward,_iHeadshotReward,_iTotalReward;
	int _iLevelGold;

	public void Call_LC()
	{
		if(SoundsHandler._instance)
		{
			SoundsHandler._instance.StopAllSounds ();
			SoundsHandler._instance.Sound_LC.Play ();
		}
		LevelComplete.gameObject.SetActive (true);
		LevelComplete.Call_TweenIn ();
		im_LCLevel.sprite = spr_Levels [StaticVariables._iCurrentLevel - 1];
		if (StaticVariables.m_iUnlockedLevels <= StaticVariables._iCurrentLevel && StaticVariables.m_iUnlockedLevels <= StaticVariables.mi_TotalLevels)
			StaticVariables.m_iUnlockedLevels += 1;
        MyDebug.Log("<color=blue>---- Unlocked Levels:::: </color>"+StaticVariables.m_iUnlockedLevels);

        if (CheckIfItsFirstTime())
        {
            _iLevelReward = StaticVariables._iCurrentLevel * 215;
            _iHeadshotReward = _iHeadShots * 100;
            _iLevelGold = _iHeadShots * 25;
            SettingLevelPrefab();
        }
        else
        {
            _iLevelReward = StaticVariables._iCurrentLevel * 50;
            _iHeadshotReward = _iHeadShots;
            _iLevelGold = _iHeadShots;
        }

		_iTotalReward = _iLevelReward + _iHeadshotReward;
		StaticVariables._iTotalCoins += _iTotalReward;
		StaticVariables._iTotalGold += _iLevelGold;
		UpdateLCRewards ();
		Check_BtnVisibility ();
#if !AdSetup_OFF
        //************************************GAME NEXA CODE************************************//
        GoogleAdMobsManager.instance.DisplayInterstitial_Win();
#endif      //END//
    }
    void UpdateLCRewards()
	{
		iTween.ValueTo (Text_ZombiesKilled.gameObject, iTween.Hash ("from", 0, "to", GameManager._instance.scr_LevelInfo.mi_DoneCount,"Onupdate","KillsUpdate", "time", 0.5f, "easetype", iTween.EaseType.linear,"Onupdatetarget",this.gameObject));
		iTween.ValueTo (Text_HeadShotReward.gameObject, iTween.Hash ("from", 0, "to", _iHeadshotReward,"Onupdate","HeadshotsUpdate", "time", 0.5f,"delay",0.25, "easetype", iTween.EaseType.linear,"Onupdatetarget",this.gameObject));
		iTween.ValueTo (Text_LevelReward.gameObject, iTween.Hash ("from", 0, "to",_iLevelReward,"Onupdate","LevelRewardUpdate", "time", 0.5f, "delay",0.5,"easetype", iTween.EaseType.linear,"Onupdatetarget",this.gameObject));
		iTween.ValueTo (Text_TotalReward.gameObject, iTween.Hash ("from", 0, "to",_iTotalReward,"Onupdate","TotalRewardUpdate", "time", 0.5f,"delay",1, "easetype", iTween.EaseType.linear,"Onupdatetarget",this.gameObject));
		iTween.ValueTo (Text_LevelGold.gameObject, iTween.Hash ("from", 0, "to",_iLevelGold,"Onupdate","LevelGoldUpdate", "time", 0.5f,"delay",1, "easetype", iTween.EaseType.linear,"Onupdatetarget",this.gameObject));
	}
	void KillsUpdate(int _val)
	{
		Text_ZombiesKilled.text = "" + _val;
	}
	void HeadshotsUpdate(int _val)
	{
		Text_HeadShotReward.text = "" + _val;
	}
	void LevelRewardUpdate(int _val)
	{
		Text_LevelReward.text = "" + _val;
	}
	void TotalRewardUpdate(int _val)
	{
		Text_TotalReward.text = "" + _val;
	}
	void LevelGoldUpdate(int _val)
	{
		Text_LevelGold.text = "" + _val;
	}
	public void Call_DbleRewardSuccess()
	{
		mg_Btn2X.SetActive (false);
		_iTotalReward += _iTotalReward;
		StaticVariables._iTotalCoins += _iTotalReward;
		UpdateLCRewards ();
	}

	public void Call_LF()
	{
#if !AdSetup_OFF
        //********************************************Game Nexa Code******************************//
        GoogleAdMobsManager.instance.DisplayInterstitial_Lose();
        //End
#endif


		if(SoundsHandler._instance)
		{
			SoundsHandler._instance.StopAllSounds ();
			SoundsHandler._instance.Sound_LF.Play ();
		}
		if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Career)
		{
			LevelFail.gameObject.SetActive (true);
			im_LFLevel.sprite = spr_Levels [StaticVariables._iCurrentLevel - 1];

			LevelFail.Call_TweenIn ();
		}
		else if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Bounty)
		{
			BountyLF.gameObject.SetActive (true);
			BountyLF.Call_TweenIn ();
		}
		Check_BtnVisibility ();
        Debug.Log("LF HERE");
    }

	public void Call_BountyLC()
	{
		BountyLC.gameObject.SetActive (true);
		BountyLC.Call_TweenIn ();
		Text_BountyKilledCount.text = "" + GameManager._instance.scr_LevelInfo.mi_DoneCount;
		_iHeadshotReward = _iHeadShots * 100;
		_iLevelGold = _iHeadShots * 25;
		Text_BountyGold.text = "" + _iLevelGold;
		Text_BountyTotalReward.text = "" + ((GameManager._instance.scr_LevelInfo.mi_DoneCount*50) + (_iHeadShots * 100));
		int _rwrd=((GameManager._instance.scr_LevelInfo.mi_DoneCount*50) + (_iHeadShots * 100));;
		StaticVariables._iTotalCoins += _rwrd;
		StaticVariables._iTotalGold += _iLevelGold;
		Check_BtnVisibility ();

#if !AdSetup_OFF
        //************************************GAME NEXA CODE************************************//
        GoogleAdMobsManager.instance.DisplayInterstitial_Win();
        //END//
#endif
    }

    public void Call_SurvivalResults()
	{
		SurvivalResults.gameObject.SetActive (true);
		SurvivalResults.Call_TweenIn ();
		Text_SurvivalKilledCount.text = "" + GameManager._instance.scr_LevelInfo.mi_DoneCount;
		Text_SurvivalHeadShotReward.text = "" + _iHeadShots*100;
		_iLevelGold = _iHeadShots * 25;
		Text_SurvivalGold.text = "" + _iLevelGold ;
		Text_SurvivalTotalReward.text = "" + ((GameManager._instance.scr_LevelInfo.mi_DoneCount*50) + (_iHeadShots * 100));
		int _rwrd=((GameManager._instance.scr_LevelInfo.mi_DoneCount*50) + (_iHeadShots * 100));;
		StaticVariables._iTotalCoins += _rwrd;
		StaticVariables._iTotalGold += _iLevelGold;
		Check_BtnVisibility ();
	}
		
	public void Call_Revive()
	{
		if (GameManager._instance.m_GameState != GameManager.e_GameState.GamePlay)
			return;
		GameManager._instance.m_GameState = GameManager.e_GameState.Revive;
		Revive.gameObject.SetActive (true);
		Revive.Call_TweenIn ();
	}
	public void Call_Pause(bool _isActive)
	{
		mg_Pause.SetActive (_isActive);
		if (_isActive) {
			GameManager._instance.m_GameState = GameManager.e_GameState.Pause;
			Time.timeScale = 0;
		}
		else {
			Time.timeScale = 1;
			GameManager._instance.m_GameState = GameManager.e_GameState.GamePlay;
		}
	}
#endregion

	void SlowMotionEnding()
	{
		im_SlowMo.GetComponent<Animator> ().enabled = true;
	}

	public void Show_Alert()
	{
		
	}

    public void yesWatchVideo()
    {
#if !AdSetup_OFF
        GoogleAdMobsManager.instance._rewardType = rewardType.grenades;
        GoogleAdMobsManager.instance.DisplayRewardVideo(0);
#endif
        //GoogleAdMobsManager.instance.RequestRewardVideo();
    }

    public void rewardVideoCompletion()
    {
        Call_Pause(false);
        noGrenadePannel.SetActive(false);
        inventory.grenade += 1;
        Update_GranedeCount();
    }
    bool CheckIfItsFirstTime()
    {
        if (StaticVariables._iCurrentLevel == 1)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level1, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 2)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level2, 0) == 0)
                return true;
            else
                return false;
        }

        else if (StaticVariables._iCurrentLevel == 3)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level3, 0) == 0)
                return true;
            else
                return false;
        }

        else if (StaticVariables._iCurrentLevel == 4)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level4, 0) == 0)
                return true;
            else
                return false;
        }

        else if (StaticVariables._iCurrentLevel == 5)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level5, 0) == 0)
                return true;
            else
                return false;
        }

        else if (StaticVariables._iCurrentLevel == 6)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level6, 0) == 0)
                return true;
            else
                return false;
        }

        else if (StaticVariables._iCurrentLevel == 7)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level7, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 8)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level8, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 9)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level9, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 10)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level10, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 11)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level11, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 12)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level12, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 13)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level13, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 14)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level14, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 15)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level15, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 16)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level16, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 17)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level17, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 18)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level18, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 19)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level19, 0) == 0)
                return true;
            else
                return false;
        }
        else if (StaticVariables._iCurrentLevel == 20)
        {
            if (PlayerPrefs.GetInt(ServicesStaticVariables.level20, 0) == 0)
                return true;
            else
                return false;
        }
        else
            return true;

    }
    void SettingLevelPrefab()
    {
        if (StaticVariables._iCurrentLevel == 1)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level1, 1);
        }
        else if (StaticVariables._iCurrentLevel == 2)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level2, 1);
        }
        else if (StaticVariables._iCurrentLevel == 3)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level3, 1);
        }
        else if (StaticVariables._iCurrentLevel == 4)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level4, 1);
        }
        else if (StaticVariables._iCurrentLevel == 5)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level5, 1);
        }
        else if (StaticVariables._iCurrentLevel == 6)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level6, 1);
        }
        else if (StaticVariables._iCurrentLevel == 7)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level7, 1);
        }
        else if (StaticVariables._iCurrentLevel == 8)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level8, 1);
        }
        else if (StaticVariables._iCurrentLevel == 9)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level9, 1);
        }
        else if (StaticVariables._iCurrentLevel == 10)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level10, 1);
        }

        else if (StaticVariables._iCurrentLevel == 11)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level11, 1);
        }
        else if (StaticVariables._iCurrentLevel == 12)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level12, 1);
        }
        else if (StaticVariables._iCurrentLevel == 13)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level13, 1);
        }
        else if (StaticVariables._iCurrentLevel == 14)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level14, 1);
        }
        else if (StaticVariables._iCurrentLevel == 15)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level15, 1);
        }
        else if (StaticVariables._iCurrentLevel == 16)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level16, 1);
        }
        else if (StaticVariables._iCurrentLevel == 17)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level17, 1);
        }
        else if (StaticVariables._iCurrentLevel == 18)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level18, 1);
        }
        else if (StaticVariables._iCurrentLevel == 19)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level19, 1);
        }
        else if (StaticVariables._iCurrentLevel == 20)
        {
            PlayerPrefs.SetInt(ServicesStaticVariables.level20, 1);
        }
    }
}
