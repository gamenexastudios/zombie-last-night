﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MySystem;

public class LevelInfo : MonoBehaviour 
{

	[System.Serializable]
	public class LevelData
	{
		public GameObject mg_ZombieObj;
		public List<ZombieBehavior> mlist_Zombies;
		public List<ZombieCreate> mlist_Crt;

		public Transform mt_PlayerPos;
		public Transform mt_AttackObj;
	}
	public LevelData[] m_LevelTargets;
	public bool mb_Movable,mb_PosSwitch=false;
	[Header("============ Checking Details ==================")]
	public int mi_CurrentWave=0;
	public int mi_Totaltargets;
	[HideInInspector] public int mi_DoneCount;
	public bool _isinSlowMotion;
	[Header("============ For Theme3 Only ==================")]
	public GameObject mg_Envi;
	void Awake()
	{
		if (mg_Envi != null)
			mg_Envi.SetActive (true);
	}
	void Start () 
	{
		
		
		mi_CurrentWave = 0;
		Set_CurrectWave ();
//		for(int i=0;i<m_LevelTargets.Length;i++)
//		{
//			for(int k=0;k<m_LevelTargets[i].mg_ZombieObj.transform.childCount;k++)
//			{
//				m_LevelTargets [i].mlist_Crt.Add (m_LevelTargets [i].mg_ZombieObj.transform.GetChild (k).GetComponent<ZombieCreate> ());
////				m_LevelTargets [i].mlist_Zombies.Add (m_LevelTargets [i].mg_ZombieObj.transform.GetChild (k).GetChild(0).GetComponent<ZombieBehavior> ());
//			}
//			/*for(int j=0;j<m_LevelTargets [i].mlist_Crt.Count;j++)
//			{
//				m_LevelTargets [i].mlist_Zombies.Add (m_LevelTargets [i].mlist_Crt[j].transform.GetChild (0).GetComponent<ZombieBehavior>()); 
//			}
//			*/
//			mi_Totaltargets += m_LevelTargets [i].mg_ZombieObj.transform.childCount;
//		}
		for(int i=0;i<m_LevelTargets.Length;i++)
		{
			mi_Totaltargets += m_LevelTargets [i].mg_ZombieObj.transform.childCount;
		}
		Debug.Log ("---- TargetCount ::::" + mi_Totaltargets);

		InGameUIHandler._instance.Update_TargetText ();
	}

	public void Set_CurrectWave()
	{
		GameManager._instance.scr_LevelInfo._isAlerted = false;
		m_LevelTargets [mi_CurrentWave].mg_ZombieObj.transform.parent.gameObject.SetActive (true);
		for(int k=0;k<m_LevelTargets[mi_CurrentWave].mg_ZombieObj.transform.childCount;k++)
		{
			m_LevelTargets [mi_CurrentWave].mlist_Crt.Add (m_LevelTargets [mi_CurrentWave].mg_ZombieObj.transform.GetChild (k).GetComponent<ZombieCreate> ());
			m_LevelTargets [mi_CurrentWave].mlist_Zombies.Add (m_LevelTargets [mi_CurrentWave].mg_ZombieObj.transform.GetChild (k).GetChild(0).GetComponent<ZombieBehavior> ());
		}
		Invoke ("Disable_PreviousWave", 2f);
	}

	void Disable_PreviousWave()
	{
		if(mi_CurrentWave!=0)
			m_LevelTargets [mi_CurrentWave-1].mg_ZombieObj.transform.parent.gameObject.SetActive (false);
	}
	bool _isAlerted;

	public void Set_AlertEnimies()
	{
		if(!_isAlerted)
		{
			_isAlerted = true;
			for(int i=0;i<m_LevelTargets[mi_CurrentWave].mlist_Zombies.Count;i++)
			{
				m_LevelTargets [mi_CurrentWave].mlist_Zombies [i].SetAlert ();
			}
		}
	}

	public void Check_LC()
	{
		if(m_LevelTargets[mi_CurrentWave].mlist_Zombies.Count<=0)
		{
			mi_CurrentWave += 1;
			if(mi_CurrentWave < m_LevelTargets.Length) 
			{

				Debug.Log ("---- Activate Next Wave");
				if(mb_Movable)
				{
					Invoke ("Set_PlayerPos", 2f);
				}
				else
					Set_CurrectWave ();
			}
			else 
			{
				if(GameManager._instance.m_GameState!=GameManager.e_GameState.LC || GameManager._instance.m_GameState!=GameManager.e_GameState.LF)
				{
					GameManager._instance.m_GameState = GameManager.e_GameState.LC;
					MyDebug.Log ("---- LC");
					if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Career)
					{


                        //************************************GAME NEXA CODE************************************//
                       // GoogleAdMobsManager.instance.RequestInterstitialAd();
                        //ENd//


                        InGameUIHandler._instance.Invoke("Call_LC",2f);
                    }
					else if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Bounty)
					{


                        //************************************GAME NEXA CODE************************************//
                       // GoogleAdMobsManager.instance.RequestInterstitialAd();
                        //END//


                        InGameUIHandler._instance.Invoke("Call_BountyLC",2f);
                    }                   
                }
			}
		}
	}

	void Set_PlayerPos()
	{
		if(m_LevelTargets[mi_CurrentWave].mt_PlayerPos!=null)
		{
			if(!mb_PosSwitch)
			{
				playercontroller._instance._isAutoMove = true;
				GameManager._instance.mg_PlayerIns.GetComponent<PlayerMovement> ().Set_MovementPropeties (true);
				StartCoroutine(InGameUIHandler._instance.mg_FadeObj.GetComponent<FadeInOut> ().Call_Fade (0.01f));
			}
			else
			{
				StartCoroutine(InGameUIHandler._instance.mg_FadeObj.GetComponent<FadeInOut> ().Call_Fade (0.01f));
				GameManager._instance.mg_PlayerIns.transform.transform.position = GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_PlayerPos.position;
				GameManager._instance.mg_PlayerIns.transform.transform.rotation = GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_PlayerPos.rotation;

				Set_CurrectWave ();
			}
		}
		else
		{
			Set_CurrectWave ();
		}
	}

	public void Enable_SlowMotion()
	{
		if (_isinSlowMotion)
			return;
		_isinSlowMotion = true;
		if(m_LevelTargets[mi_CurrentWave].mlist_Zombies.Count>0)
		{
			for(int i=0;i<m_LevelTargets[mi_CurrentWave].mlist_Zombies.Count;i++)
			{
				m_LevelTargets [mi_CurrentWave].mlist_Zombies [i].Call_SlowMotion (true);
			}
			Invoke ("Disable_SlowMotion",6f);
		}
	}

	void Disable_SlowMotion()
	{
		_isinSlowMotion = false;
		if(GameManager._instance.m_GameState==GameManager.e_GameState.LC)
		{
			mi_CurrentWave = mi_CurrentWave - 1;
		}
		if(m_LevelTargets[mi_CurrentWave].mlist_Zombies.Count>=0)
		{
			for(int i=0;i<m_LevelTargets[mi_CurrentWave].mlist_Zombies.Count;i++)
			{
				m_LevelTargets [mi_CurrentWave].mlist_Zombies [i].Call_SlowMotion (false);
			}
			InGameUIHandler._instance.im_SlowMo.enabled = false;
			InGameUIHandler._instance.im_SlowMo.GetComponent<Animator> ().enabled = false;
		}
	}
}
