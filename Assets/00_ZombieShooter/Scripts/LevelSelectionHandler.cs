﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectionHandler : MonoBehaviour 
{
	public static LevelSelectionHandler _instance;
	int _iUnlockedLevels;
    public GameObject ScrollViewContent;
	public Image im_Level;
	public Sprite[] spr_Levels;
    public GameObject[] LevelObjs;
    public Text[] Text_LevelNo;

	public Text Text_MissionDetails,Text_LevelStatus,Text_LevelReward;
	public Text Text_GranedeCount,Text_HealthCount,Text_SlowMoCount;
	public Button Btn_Next, Btn_Prev;
	int _iLevelID,_iGranedeCount,_iHealthCount,_iSlowMoCount;


    public GameObject _wannaBuyGrenade;
    public GameObject _wanaBuyHealth;
    public GameObject _wanaBuyTimer;
	public enum e_ConsumableType
	{
		Granede,
		Health,
		SlowMo
	}
	[HideInInspector]public e_ConsumableType m_ConsumableType;
	void Awake()
	{
		_instance = this;
	}

	void Start()
	{
		_iUnlockedLevels = StaticVariables.m_iUnlockedLevels;
		_iLevelID = _iUnlockedLevels;
		Debug.Log ("---- Unlocked Levels::::" + _iUnlockedLevels);
		Check_LevelProperties ();

		_iGranedeCount = StaticVariables._iGranedeCount;
		_iHealthCount = StaticVariables._iHealthCount;
		_iSlowMoCount = StaticVariables._iSlowMotionCount;
		Text_GranedeCount.text = "" + _iGranedeCount;
		Text_HealthCount.text = "" + _iHealthCount;
		Text_SlowMoCount.text = "" + _iSlowMoCount;

        SetScrollPos();

    }

	void Check_LevelProperties()
	{
		iTween.ScaleFrom (im_Level.gameObject, iTween.Hash ("Y", 0, "time", 0.15f, "easetype", iTween.EaseType.spring));
		iTween.RotateFrom (im_Level.gameObject, iTween.Hash ("Z", 45, "time", 0.15f, "easetype", iTween.EaseType.spring));

		im_Level.sprite = spr_Levels [_iLevelID-1];
		Text_MissionDetails.text = "MISSION " + _iLevelID + " / " + 20;
//		Text_LevelReward.text = " " + 250 * _iLevelID;
		int _reward=250 * _iLevelID;
		iTween.ValueTo (Text_LevelReward.gameObject, iTween.Hash ("from", _reward-250, "to", _reward, "time", 0.5f, "Onupdate", "UpdateLvlReward", "OnupdateTarget", this.gameObject));
		if(_iLevelID<=1)
		{
			Btn_Prev.interactable = false;
			Btn_Next.interactable = true;
		}
		else
		{
			Btn_Prev.interactable = true;
		}
		if(_iLevelID>=20)
		{
			Btn_Prev.interactable = true;
			Btn_Next.interactable = false;
		}
		else
		{
			Btn_Next.interactable = true;
		}
		if(_iLevelID<=StaticVariables.m_iUnlockedLevels)
        {
			Text_LevelStatus.text = "START";
			im_Level.color = new Color (1, 1, 1, 1);
		}
		else
        {
			Text_LevelStatus.text = "LOCKED";
			im_Level.color = new Color (1, 1, 1, 0.5f);
		}

        for (int i = 0; i < LevelObjs.Length; i++)
        {
            if (i < StaticVariables.m_iUnlockedLevels)
            {
                LevelObjs[i].gameObject.GetComponent<Image>().color = new Color(LevelObjs[i].gameObject.GetComponent<Image>().color.r,
                    LevelObjs[i].gameObject.GetComponent<Image>().color.g, LevelObjs[i].gameObject.GetComponent<Image>().color.b, 1f);

                Text_LevelNo[i].text = "MISSION " + (i + 1);

                LevelObjs[i].GetComponent<Button>().interactable = true;
            }
            else
            {
                //LevelObjs[i].gameObject.GetComponent<Image>().color = new Color(LevelObjs[i].gameObject.GetComponent<Image>().color.r,
                //    LevelObjs[i].gameObject.GetComponent<Image>().color.g, LevelObjs[i].gameObject.GetComponent<Image>().color.b, 0.5f);

                Text_LevelNo[i].text = "LOCKED";

                LevelObjs[i].GetComponent<Button>().interactable = false;
            }

        }
	}

	void UpdateLvlReward(int _val)
	{
		Text_LevelReward.text = ""+ _val+" COINS";
	}

	public void OnBtnClick(string _str)
	{
		if (SoundsHandler._instance)
			SoundsHandler._instance.Sound_BtnClick.Play ();
		switch(_str)
		{
		case "Btn_NextLevel":
			if(_iLevelID<20)
			{
				_iLevelID++;
				Check_LevelProperties ();
			}
			break;
		case "Btn_PrevLevel":
			if(_iLevelID>1)
			{
				_iLevelID--;
				Check_LevelProperties ();
			}
			break;
		case "Btn_Start":
			if(_iLevelID<=StaticVariables.m_iUnlockedLevels)
			{
				Debug.Log ("---- Selected Lvl ::::" + _iLevelID);
				StaticVariables._iCurrentLevel=_iLevelID;
				MenuHandler._instance.Call_Upgrade (true);
                    Tween_LS._instance.Call_TweenOut();
			}
			else
            {
				Debug.Log ("---- Locked Lvl :::: Call Inapp");
			}
			break;

		case "Btn_BuyGranede":
			Check_ConsumablePurchase (e_ConsumableType.Granede);
			break;
		case "Btn_BuyHealth":
			Check_ConsumablePurchase (e_ConsumableType.Health);
			break;
		case "Btn_BuySlowMo":
			Check_ConsumablePurchase (e_ConsumableType.SlowMo);
			break;
		}
	}


	void Check_ConsumablePurchase(e_ConsumableType _type)
	{
		if(_type == e_ConsumableType.Granede)
		{
			if (StaticVariables._iTotalCoins >= 1000) 
			{
                _wannaBuyGrenade.SetActive(true);
			}
			else
			{
				MenuHandler._instance.OnBtnClick ("Btn_ItemPacks");
			}
		}
		else if(_type == e_ConsumableType.Health)
		{
			if (StaticVariables._iTotalCoins >= 1500) 
            {
                _wanaBuyHealth.SetActive(true);	
			}
			else
			{
				MenuHandler._instance.OnBtnClick ("Btn_ItemPacks");
			}
		}
		else if(_type == e_ConsumableType.SlowMo)
		{
			if (StaticVariables._iTotalCoins >= 2000)
            {
                _wanaBuyTimer.SetActive(true);	
			}
			else
			{
				MenuHandler._instance.OnBtnClick ("Btn_ItemPacks");
			}
		}
	}

	public static void UnlockAllLevels()
	{
		StaticVariables.m_iUnlockedLevels = 20;
	}

    public void SelectLevel(int _leveli)
    {
        Debug.Log("Selected Level : " + _leveli);
        //FirebaseEvents.instance.LogFirebaseEvent("Funnel_CareerPlayLevelSelection", "Level: " +_leveli, "CareerPlayLevelSelection Button");
        if (_iLevelID <= StaticVariables.m_iUnlockedLevels)
        {
            Debug.Log("---- Selected Lvl ::::" + _leveli);
            StaticVariables._iCurrentLevel = _leveli;
            MenuHandler._instance.Call_Upgrade(true);
            Tween_LS._instance.Call_TweenOut();
            Tween_Upgrade._instance.Call_TweenIn();
        }
    }

    public void SetScrollPos()
    {
        if(StaticVariables.m_iUnlockedLevels<3)
        ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.00f;

        if (StaticVariables.m_iUnlockedLevels > 3 && StaticVariables.m_iUnlockedLevels <=6)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.06f*3;

        if (StaticVariables.m_iUnlockedLevels >6 && StaticVariables.m_iUnlockedLevels <=9)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.06f * 6;

        if (StaticVariables.m_iUnlockedLevels >= 10 && StaticVariables.m_iUnlockedLevels <=12)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.06f * 9;

        if (StaticVariables.m_iUnlockedLevels >= 13 && StaticVariables.m_iUnlockedLevels <= 15)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.0585f * 12;

        if (StaticVariables.m_iUnlockedLevels >= 16 && StaticVariables.m_iUnlockedLevels <= 18)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.05845f * 15;

        if (StaticVariables.m_iUnlockedLevels >= 19 && StaticVariables.m_iUnlockedLevels <= 20)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.05845f * 17;

        if (StaticVariables.m_iUnlockedLevels >= 21 && StaticVariables.m_iUnlockedLevels <= 22)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.05845f * 19;

        if (StaticVariables.m_iUnlockedLevels >= 23 && StaticVariables.m_iUnlockedLevels <= 24)
            ScrollViewContent.GetComponent<ScrollRect>().horizontalScrollbar.value = 0.05845f * 21;
    }


    public void OkBuyGrenade()
    {
        StaticVariables._iTotalCoins -= 1000;
        StaticVariables._iGranedeCount += 1;
        Text_GranedeCount.text = "" + StaticVariables._iGranedeCount;
        _wannaBuyGrenade.SetActive(false);
    }

    public void OkBuyHealth()
    {
        StaticVariables._iTotalCoins -= 1500;
        StaticVariables._iHealthCount += 1;
        Text_HealthCount.text = "" + StaticVariables._iHealthCount;
        _wanaBuyHealth.SetActive(false);
    }

    public void OkBuyTimer()
    {
        StaticVariables._iTotalCoins -= 2000;
        StaticVariables._iSlowMotionCount += 1;
        Text_SlowMoCount.text = "" + StaticVariables._iSlowMotionCount;
        _wanaBuyTimer.SetActive(false);
    }

    public void Cancel()
    {
        _wannaBuyGrenade.SetActive(false);
        _wanaBuyHealth.SetActive(false);
        _wanaBuyTimer.SetActive(false);
    }
}
