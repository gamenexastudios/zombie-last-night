﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MySystem;

public class MenuHandler : MonoBehaviour 
{
	public static MenuHandler _instance;
	public GameObject mg_TitlePage;
	public PageAnimHandler[] UiObj;
	public GameObject[] mg_UpgradeObjs;
	public Text Text_SoundStatus;
	public Image im_Sound;
	public Sprite spr_SoundOn,spr_SoundOff;

//	public GameObject mg_FreeCashObj;
	bool _isinFreeCash;
	[Header("============= Packs Region =============")]
	public GameObject mg_GoldPacks;
	public GameObject mg_CashPacks, mg_ItemsPack;
	public Image im_Cash, im_Gold, im_Item;
	public Sprite spr_CashOn,spr_Cashoff,spr_GoldOn,spr_Goldoff,spr_ItemOn,spr_Itemoff;
	[Header("============= ModeSel Region =============")]
	public Text Text_SurvivalStatus;
	public Text Text_BountyStatus;
	public Button Btn_Survival,Btn_Bounty;
	[Header("============= Rank Region =============")]
	public Text[] Text_Rank;
	public Image[] im_rankFillBar;
	public Image[] im_Rankbadge;
	public Sprite[] spr_Badges;
	public GameObject mg_RankPage,mg_BtnRankClaim;
	public Image im_Rank;
	[Header("============= LOCALIZATION REGION =============")]
	public Text Text_CountryName;
	public Image im_Country;
	public Sprite[] spr_Country;
	public string[] ms_LanguageCode,ms_CountryName;
	int _iLanguageID = 0;

	[Header("============= EXTRA =============")]
	public GameObject mg_FreeCash;
	public GameObject mg_PanelFreeCash;

    public GameObject[] Btn_UnlockAllLevles;

    public GameObject PacksScrollView;

    public GameObject menu;
    public GameObject image;


    public int PlayButtonAd;
    public int ModeSelectionAd;

    void Awake()
	{
		_instance = this;
        
	}

	void Start () 
	{
        //Custom code
        if (ServicesStaticVariables._homeButtonPressed == true)
        {
            menu.SetActive(true);
            ServicesStaticVariables._homeButtonPressed = false;
        }
        else if (ServicesStaticVariables._retryButtonPressed == true)
        {
            menu.SetActive(false);
            ServicesStaticVariables._retryButtonPressed = false;
        }
        else
        {
            menu.SetActive(true);
        }
        image.GetComponent<Image>().color = new Vector4(1, 1, 1, 1);

		Debug.Log("---- Enable Page ID ::::"+ StaticVariables._iEnablePageID);
        if (StaticVariables._iEnablePageID > 10)
        {
            mg_TitlePage.SetActive(true);
            EnableUIPage(0);
            Tween_Menu._instance.Call_TweenIn();
        }
        else
        {
            EnableUIPage(StaticVariables._iEnablePageID);
            if (StaticVariables._iEnablePageID == 5)
            {
                // Check_Rank();
                EnableUIPage(5);
                Tween_LS._instance.Call_TweenIn();
            }
        }
		PackID = 0;

		if(StaticVariables.m_iUnlockedLevels>=4)
		{
			Text_SurvivalStatus.gameObject.SetActive (false);
			Btn_Survival.interactable = true;
		}
		if(StaticVariables.m_iUnlockedLevels>=6)
		{
			Text_BountyStatus.gameObject.SetActive (false);
			Btn_Bounty.interactable = true;
		}
		Update_RanksInfo ();
		AudioListener.volume = StaticVariables.iSoundStatus;
		if (StaticVariables.iSoundStatus == 1) {
//			Text_SoundStatus.text = "Sound On";
			Text_SoundStatus.text = ms_Soundstrings[0];
			im_Sound.sprite = spr_SoundOn;
		} else {
			Text_SoundStatus.text = ms_Soundstrings[1];
//			Text_SoundStatus.text = "Sound Off";
			im_Sound.sprite = spr_SoundOff;
		}

        //if (LanguageList != null)
        //{
        //Check_SelectedLanguage(); // RS
        //}

        Check_BtnVisibility();
    }

    void Check_BtnVisibility()
    {
        if (StaticVariables.m_iUnlockedLevels >= 20)
        {
            for (int i = 0; i < Btn_UnlockAllLevles.Length; i++)
                Btn_UnlockAllLevles[i].SetActive(false);
        }
        //if (StaticVariables.strUnlockedWeapons == StaticVariables.strUnlockedWeaponsRef)
        //    for (int i = 0; i < mg_BtnUnlockAllUpgrades.Length; i++)
        //        mg_BtnUnlockAllUpgrades[i].SetActive(false);
    }

    public void IAPSuccess(int _id)
    {
        //switch (_id)
        //{
        //    case 0:
        //        //NoAds
        //        AdsFreeGame = true;
        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsNoAds, "True");
        //        break;
        //    case 1:
        //        LevelSelectionHandler.UnlockAllLevels();
        //        UnlckdAllLevels = true;
        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsDiscount75Purchased, "True");
        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsDiscount50Purchased, "True");
        //        break;
        //    case 2:
        //        UpgradeManager.UnlockAllUpgrades();
        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsDiscount75Purchased, "True");
        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsDiscount50Purchased, "True");
        //        UnlckdAllUpgrades = true;
        //        MyDebug.Log("Guns :::unlocked:: " + UnlckdAllUpgrades);
        //        break;
        //    case 3:
        //        LevelSelectionHandler.UnlockAllLevels();
        //        UpgradeManager.UnlockAllUpgrades();

        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsDiscount75Purchased, "True");
        //        PlayerPrefs.SetString(MyAdsGamePrefs.IsDiscount50Purchased, "True");
        //        break;
        //    // Cash Packs
        //    case 4: //Basic Cash
        //        Set_Coins(2000);
        //        break;
        //    case 5: //Mini Cash
        //        Set_Coins(5000);
        //        break;
        //    case 6: //Regular Cash
        //        Set_Coins(10000);
        //        break;
        //    case 7: //Mega Cash
        //        Set_Coins(18500);
        //        break;
        //    case 8: //Booster Cash
        //        Set_Coins(25000);
        //        break;
        //    case 9: //Ultra Cash
        //        Set_Coins(50000);
        //        break;
        //    case 10: //Pro Cash
        //        Set_Coins(75000);
        //        break;
        //    case 11: //Jumbo Cash
        //        Set_Coins(100000);
        //        break;

        //    // GOLD Packs
        //    case 12: //Basic Gold
        //        Set_Gold(450);
        //        break;
        //    case 13: //Mini Gold
        //        Set_Gold(1000);
        //        break;
        //    case 14: //Regular Gold
        //        Set_Gold(1650);
        //        break;
        //    case 15: //Mega Gold
        //        Set_Gold(2050);
        //        break;
        //    case 16: //Booster Gold
        //        Set_Gold(2500);
        //        break;
        //    case 17: //Ultra Gold
        //        Set_Gold(5000);
        //        break;
        //    case 18: //Pro Gold
        //        Set_Gold(12500);
        //        break;
        //    case 19: //Jumbo Gold
        //        Set_Gold(18500);
        //        break;

        //    // Consumables
        //    case 20:  // Healthkit_1
        //        StaticVariables._iHealthCount += 6;
        //        break;
        //    case 21:  // GranedePack_1
        //        StaticVariables._iGranedeCount += 8;
        //        break;
        //    case 22:  // SlowMoPack_1
        //        StaticVariables._iSlowMotionCount += 10;
        //        break;
        //    case 23:  // Healthkit_2
        //        StaticVariables._iHealthCount += 14;
        //        break;
        //    case 24:  // GranedePack_2
        //        StaticVariables._iGranedeCount += 18;
        //        break;
        //    case 25:  // SlowMoPack_2
        //        StaticVariables._iSlowMotionCount += 20;
        //        break;
        //    case 26:  // GranedePack_3
        //        StaticVariables._iGranedeCount += 30;
        //        break;
        //    case 27:  // Healthkit_3
        //        StaticVariables._iHealthCount += 40;
        //        break;
        //    case 28:  // 50% off
        //        break;
        //    case 29:  // 75% off 
        //        break;
        //}
    }

    public void Check_SelectedLanguage()
    {       
        _iLanguageID = LocalizationManager.Instance.GetCountryIndex();

        Set_CountryDetails();
    }
    public string[] ms_Soundstrings;

	int PackID;
	public static bool[] mb_Ranks = new bool[10];
	void Check_Rank()
	{
		if(StaticVariables.m_iUnlockedLevels % 2 == 0)
		{
			if(StaticVariables._iRank >= 1 && !mb_Ranks[(StaticVariables.m_iUnlockedLevels/2)-1])
			{
				StaticVariables._iRank -= 1;
				mb_Ranks [(StaticVariables.m_iUnlockedLevels / 2)-1] = true;
				Invoke("Call_RankEffect",1f);
				StaticVariables._iTotalGold += 3;
//				Debug.Log ("---- My Value :::: " + _val);

				Update_RanksInfo ();
			}
			else
			{
				Debug.Log ("---- RANK:::: " + StaticVariables._iRank);
				Update_RanksInfo ();
//				for(int i =0;i<Text_Rank.Length;i++)
//				{
//					Text_Rank[i].text = "Rank " + StaticVariables._iRank;
//
////					im_rankFillBar[i].fillAmount = 1;
////					im_Rankbadge[i].sprite=spr_Badges[StaticVariables._iRank];
//				}
			}
		}
	}
	void Update_RanksInfo()
	{
		float _val = (float)StaticVariables._iRank/10;

		for(int i =0;i<Text_Rank.Length;i++)
		{
			Text_Rank[i].text = "Rank " + StaticVariables._iRank;

			im_rankFillBar[i].fillAmount = 1- _val;
			im_Rankbadge[i].sprite=spr_Badges[StaticVariables._iRank-1];
		}
	}
	public void Call_RankEffect()
	{
		Debug.Log ("---- Rank Effect");
		mg_RankPage.SetActive (true);
		im_Rank.sprite = spr_Badges [StaticVariables._iRank - 1];
		iTween.ScaleFrom (im_Rank.gameObject, iTween.Hash ("Scale", Vector3.zero, "Time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
		iTween.ScaleFrom (mg_BtnRankClaim.gameObject, iTween.Hash ("Scale", Vector3.zero,"Delay",0.25f, "Time", 0.25f, "easetype", iTween.EaseType.easeOutBack));

//		iTween.ScaleTo (im_Rank.gameObject, iTween.Hash ("Scale", Vector3.zero,"Delay",3f, "Time", 0.5f, "easetype", iTween.EaseType.linear));
	}

	public void Close_RankPage()
	{
		mg_RankPage.SetActive (false);
	}

	public void Call_FreeCashPopup()
	{
		mg_FreeCash.SetActive (true);
		iTween.ScaleFrom (mg_PanelFreeCash.gameObject, iTween.Hash ("Scale", Vector3.zero, "Time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
	}
	public void Close_FreeCashPopup()
	{
		mg_FreeCash.SetActive (false);
	}
	public void OnBtnClick(string _str)
	{
		if (SoundsHandler._instance)
			SoundsHandler._instance.Sound_BtnClick.Play ();
		switch(_str)
		{
		case "Btn_GotoMenu":
			mg_TitlePage.SetActive (false);
			MyDebug.Log ("INIT MENU HERE");
			EnableUIPage (0);
            break;
		#region MenuPage
		case "Btn_Play":
                #if !AdSetup_OFF
                //FirebaseEvents.instance.LogFirebaseEvent("Funnel_PlayButton", "Scene: " + SceneManager.GetActiveScene().name, "Play Button");
                if (PlayButtonAd == 1)
                    GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif
                PackID =4;
			EnableUIPage(4);
                Tween_ModeSel._instance.Call_TweenIn();
			break;
		case "Btn_Settings":
                //FirebaseEvents.instance.LogFirebaseEvent("Funnel_SettingsButton", "Scene: " + SceneManager.GetActiveScene().name, "Settings Button");
                EnableUIPage(1);
                Tween_Settings._instance.Call_TweenIn();
			break;
		case "Btn_Store":
                //FirebaseEvents.instance.LogFirebaseEvent("Funnel_StoreButton","Scene: " + SceneManager.GetActiveScene().name,"Store Button");
			EnableUIPage(6);
                Tween_Store._instance.Call_TweenIn();
                break;
		case "Btn_FreeCash":
//			_isinFreeCash=!_isinFreeCash;
			Call_FreeCashPopup();
//			if(_isinFreeCash)
//				iTween.ScaleTo(mg_FreeCashObj.gameObject,iTween.Hash("y",1,"time",0.5f,"easetype",iTween.EaseType.easeOutBack));
//			else
//				iTween.ScaleTo(mg_FreeCashObj.gameObject,iTween.Hash("y",0,"time",0.25f,"easetype",iTween.EaseType.easeInBack));
			break;
		case "Btn_GoldPacks":
			EnableUIPage(2);
			//mg_GoldPacks.SetActive(true);
			//mg_CashPacks.SetActive(false);
			//mg_ItemsPack.SetActive(false);
			//im_Gold.sprite=spr_GoldOn;
			//im_Cash.sprite=spr_Cashoff;
			//im_Item.sprite=spr_Itemoff;
//			EnableUIPage(2);
			if(PackID==100)
				Call_Upgrade(false);

                Tween_Packs._instance.Call_TweenIn();

                PacksScrollView.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

                break;
		case "Btn_CashPacks":
			EnableUIPage(2);
                //mg_GoldPacks.SetActive(false);
                //mg_CashPacks.SetActive(true);
                //mg_ItemsPack.SetActive(false);
                //im_Gold.sprite=spr_Goldoff;
                //im_Cash.sprite=spr_CashOn;
                //im_Item.sprite=spr_Itemoff;
                //			EnableUIPage(3);

                PacksScrollView.GetComponent<RectTransform>().localPosition = new Vector3(0, 300, 0);

            if (PackID==100)
				Call_Upgrade(false);

                Tween_Packs._instance.Call_TweenIn();
                break;
		case "Btn_ItemPacks":
			EnableUIPage(2);
                //mg_GoldPacks.SetActive(false);
                //mg_CashPacks.SetActive(false);
                //mg_ItemsPack.SetActive(true);
                //im_Gold.sprite=spr_Goldoff;
                //im_Cash.sprite=spr_Cashoff;
                //im_Item.sprite=spr_ItemOn;

                PacksScrollView.GetComponent<RectTransform>().localPosition = new Vector3(0, 600, 0);

                if (PackID==100)
				Call_Upgrade(false);

                Tween_Packs._instance.Call_TweenIn();
                break;
		case "Btn_MG":
			break;
#endregion

		case"Btn_Sound":
//			if (StaticVariables.iSoundStatus == 1) {
//				StaticVariables.iSoundStatus = 0;
//				Text_SoundStatus.text = "Sound On";
//				im_Sound.sprite = spr_SoundOn;
//			} else {
//				StaticVariables.iSoundStatus = 1;
//				Text_SoundStatus.text = "Sound Off";
//				im_Sound.sprite = spr_SoundOff;
//			}
			if (StaticVariables.iSoundStatus == 1) {
				StaticVariables.iSoundStatus = 0;
				Text_SoundStatus.text = ms_Soundstrings[1];
//				Text_SoundStatus.text = "Sound Off";
				im_Sound.sprite = spr_SoundOff;
			} else {
				StaticVariables.iSoundStatus = 1;
				Text_SoundStatus.text = ms_Soundstrings[0];
//				Text_SoundStatus.text = "Sound On";
				im_Sound.sprite = spr_SoundOn;
			}
			AudioListener.volume = StaticVariables.iSoundStatus;
			break;

		case "Btn_CloseSettings":
			EnableUIPage(0);
                Tween_Menu._instance.Call_TweenIn();
			break;
		case "Btn_CloseGoldPacks":
			if(PackID==100)
			{
                   // SceneManager.LoadScene("Menu");
				Call_Upgrade (true);
    //                if(UiObj[2])
				//UiObj [2].Call_TweenOut ();
				//UiObj [2].gameObject.SetActive (false);
				//UiObj [3].Call_TweenOut ();
				//UiObj [3].gameObject.SetActive (false);
			}
			else
			{
				Debug.Log ("---- Pack ID :::: " + PackID);
				EnableUIPage(PackID);
			}
			break;
#region ModeSelection
		case "Btn_Career":
                //FirebaseEvents.instance.LogFirebaseEvent("Funnel_CareerPlay", "Scene: " + SceneManager.GetActiveScene().name, "CareerPlay Button");
                StaticVariables.m_GameMode=StaticVariables.e_GameMode.Career;
			PackID=5;
			//EnableUIPage(5);
                Tween_ModeSel._instance.Call_TweenOut();
                Tween_LS._instance.Call_TweenIn();

#if !AdSetup_OFF
                if (ModeSelectionAd == 1)
                    GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif
			break;
		case "Btn_Survival":
#if !AdSetup_OFF
                if (ModeSelectionAd == 1)
                    GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif

               // FirebaseEvents.instance.LogFirebaseEvent("Funnel_SurvivalPlay", "Scene: " + SceneManager.GetActiveScene().name, "SurvivalPlay Button");
                StaticVariables.m_GameMode=StaticVariables.e_GameMode.Survival;
//			int _rand=Random.Range(0,4);
//			if(_rand==0)
				StaticVariables._sSceneToLoad= "Theme_1";
                //			else if(_rand==1)
                //				StaticVariables._sSceneToLoad="Theme_2AA";
                //			else 
                //				StaticVariables._sSceneToLoad="Theme_3BNew";



                SceneManager.LoadScene ("LoadingScene");
			break;
		case "Btn_Bounty":
#if !AdSetup_OFF
                if (ModeSelectionAd == 1)
                    GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif

                //FirebaseEvents.instance.LogFirebaseEvent("Funnel_BountyPlay", "Scene: " + SceneManager.GetActiveScene().name, "BountyPlay Button");
                StaticVariables.m_GameMode=StaticVariables.e_GameMode.Bounty;
			int _randnum=Random.Range(0,2);
//			if(_randnum==0)
//				StaticVariables._sSceneToLoad="BountySet_1";
//			else
				StaticVariables._sSceneToLoad="BountySet_2_AfterBaking";
			SceneManager.LoadScene ("LoadingScene");
			break;
		case "Btn_MSBack":
			EnableUIPage(0);
            Tween_Menu._instance.Call_TweenIn();
			break;
#endregion

#region LevelSelection
		case "Btn_LSBack":
			PackID=4;
			EnableUIPage(4);
            Tween_LS._instance.Call_TweenOut();
                Tween_ModeSel._instance.Call_TweenIn();
                break;
#endregion

#region Localization
		case "Btn_NextLang":
			if(_iLanguageID < spr_Country.Length-1)
			{
				_iLanguageID+=1;
				Set_CountryDetails();
                MyDebug.Log("Selected Country Code : "+LocalizationManager.Instance.GetCountryIndex());
			}
			break;
		case "Btn_PrevLang":
			if(_iLanguageID>0)
			{
				_iLanguageID-=1;
				Set_CountryDetails();
                MyDebug.Log("Selected Country Code : " + LocalizationManager.Instance.GetCountryIndex());
            }
                break;

#endregion
		}
	}
	void Set_CountryDetails()
	{
		im_Country.sprite=spr_Country[_iLanguageID];
		Text_CountryName.text=ms_CountryName[_iLanguageID];
		LocalizationManager.Instance.SetLanguage(ms_LanguageCode[_iLanguageID]);
	}
	public void SelectLevel(int _val)
	{
		StaticVariables._iCurrentLevel = _val;
//		UiObj [5].gameObject.SetActive (false);
//		Call_Upgrade (true);
//		StaticVariables._sSceneToLoad="Upgrade";
//		SceneManager.LoadScene ("LoadingScene");
	}

	public void Call_Upgrade(bool _status)
	{
		for(int i=0;i<mg_UpgradeObjs.Length;i++)
		{
			mg_UpgradeObjs[i].SetActive (_status);
		}
		if(_status==true) 
		{
			PackID = 100;
			//UiObj [5].gameObject.SetActive (false);      
            MyDebug.Log("UPGRADE HERE");
        }
	}

	//0-Menu, 1-Settings, 2-GoldPacks, 3-CashPacks, 4-ModeSelction, 5-LevelSelection
	public void EnableUIPage(int ID)
	{
//		PackID = ID;
//		Debug.Log("---- PAGE ID::::"+ID);
		for (int i=0;i<UiObj.Length;i++)
		{
			//UiObj [i].Call_TweenOut ();
			//UiObj [i].gameObject.SetActive (false);
		}
		//UiObj [ID].gameObject.SetActive (true);
		//UiObj [ID].Call_TweenIn ();
        MyDebug.Log("PAGE_"+ID);
        if (ID == 0)
        {
            MyDebug.Log("MENU HERE");
			if (SoundsHandler._instance)
            {
				SoundsHandler._instance.StopAllSounds ();
				SoundsHandler._instance.Sound_MenuBG.Play ();
			}
            //Tween_Menu._instance.Call_TweenIn();
        }
        else
        if (ID == 4)
        {
            MyDebug.Log("MODE SEL HERE");
        }
        else
        if (ID == 5)
        {
            MyDebug.Log("LEVEL SELECTION HERE");
        }
    }
}
