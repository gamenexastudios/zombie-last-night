﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MySystem;
using UnityEditor;

public class MySystemEditor : MonoBehaviour 
{
    [MenuItem("EditorTool/MyDebug Log/Enable")]
    private static void CheckEnable()
    {
        if (EditorUtility.DisplayDialog("EditorTool", "Are you sure? \nDo you want to Enable MyDebug Log??", "Yes", "No"))
        {
            PlayerPrefs.SetString("islog", "true");
            EditorUtility.DisplayDialog("EditorTool", "MyDebug Log Enabled", "Ok");
        }
    }

    [MenuItem("EditorTool/MyDebug Log/Enable", true)]
    private static bool Enable()
    {
        return PlayerPrefs.GetString("islog") != "true";
    }

    [MenuItem("EditorTool/MyDebug Log/Disable")]
    private static void CheckDisable()
    {
        if (EditorUtility.DisplayDialog("EditorTool", "Are you sure? \nDo you want to Disable MyDebug Log??", "Yes", "No"))
        {
            PlayerPrefs.SetString("islog", "false");
            EditorUtility.DisplayDialog("EditorTool", "MyDebug Log Disabled", "Ok");
        }
    }

    [MenuItem("EditorTool/MyDebug Log/Disable", true)]
    private static bool Disable()
    {
        return PlayerPrefs.GetString("islog") == "true";
    }


    //[MenuItem("EditorTool/MyDebug Log/Enable")]
    //private static void CheckEnable()
    //{       
    //    if (EditorUtility.DisplayDialog("EditorTool", "Are you sure? \nDo you want to Enable MyDebug Log??", "Yes", "No"))
    //    {
    //        PlayerPrefs.SetString("islog", "true");
    //        EditorUtility.DisplayDialog("EditorTool", "MyDebug Log Enabled", "Ok");
    //    }
    //}

    //[MenuItem("EditorTool/My MyDebug Log/Enable",true)]
    //private static bool Enable()
    //{
    //    return PlayerPrefs.GetString("islog") != "true";
    //}

    //[MenuItem("EditorTool/My MyDebug Log/Disable")]
    //private static void CheckDisable()
    //{
    //    if (EditorUtility.DisplayDialog("EditorTool", "Are you sure? \nDo you want to Disable MyDebug Log??", "Yes", "No"))
    //    {
    //        PlayerPrefs.SetString("islog", "false");
    //        EditorUtility.DisplayDialog("EditorTool", "MyDebug Log Disabled", "Ok");
    //    }
    //}

    //[MenuItem("EditorTool/My MyDebug Log/Disable", true)]
    //private static bool Disable()
    //{       
    //    return PlayerPrefs.GetString("islog") == "true";
    //}
}
