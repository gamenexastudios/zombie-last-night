﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour 
{
	public static PlayerMovement _instance;

	public bool _canMove;
	public float _fSpeed = 5f;

	void Update () 
	{
		if(_canMove)
		{
			transform.position = Vector3.MoveTowards (transform.position, GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_PlayerPos.position, _fSpeed * Time.deltaTime);
			Camera.main.transform.rotation = GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_PlayerPos.rotation;
		}

		if(_canMove==true &&  transform.position == GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_PlayerPos.position)
		{
			_canMove = false;
			playercontroller._instance._isAutoMove = false;
			Set_MovementPropeties (false);
		}
	}

	public void Set_MovementPropeties(bool _status)
	{
		_canMove = _status;
		Camera.main.GetComponent<CameraControllesAssaults> ().enabled = !_status;
		if(InGameUIHandler._instance.mb_Zoom)
		{
			InGameUIHandler._instance.mb_Zoom = false;
		}
		if(!_status)
			GameManager._instance.scr_LevelInfo.Set_CurrectWave ();
	}
}
