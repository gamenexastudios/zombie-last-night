﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MySystem;

public class PopUpHandler : MonoBehaviour 
{
	public static PopUpHandler _myScript;
    public static PopUpHandler myScript
    {
        get
        {
            if (_myScript == null)
            {
                _myScript = FindObjectOfType<PopUpHandler>();
            }
            return _myScript;
        }
    }

    public GameObject PopUp_AlphaBG;

    public GameObject PopUp_WelComeReward;
    public GameObject PopUp_WelComeReward2;
    public GameObject Btn_WC2XClose;
    public Text Text_Welcome,Text_WelCome2x;

    public GameObject PopUp_UnlockAllUpgrades;
    public GameObject PopUp_UnlockAllLevels;

    public GameObject PopUp_50Disc,Btn50Close;
    public GameObject PopUp_75Disc, Btn75Close;
    public Text Text_50Disc, Text_75Disc;

    public static string Is_WelComeRewardTaken= "Is_WelComeRewardTakenA";

    public GameObject PopUp_2XLCReward;
    public GameObject Btn_2XLCRewardClose;

    public GameObject PopUp_2XDailyBonus;
    public GameObject Btn_2XDailyBonusClose;

 //   void Awake()
	//{
		
	//}

	//void Start () 
	//{
 //       Text_Welcome.text = "Thank You for Downloading\n"+Application.productName+"!\nPlease Collect your Welcome Bonus of 1000 "+MyAdsManager.myScript.RewardType+"!!";
 //       Text_WelCome2x.text = "More Free"+MyAdsManager.myScript.RewardType+"\nMake it Double by Watching Video!!";
 //   }
	
 //   public void PopUpWelcome_In()
 //   {
 //       MyDebug.Log("[PH] PopUpWelcome_In");
 //       if (!PlayerPrefs.HasKey(Is_WelComeRewardTaken))
 //       {
 //           AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //           iTween.ScaleTo(PopUp_WelComeReward.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       }
 //       else
 //       {
 //           if(DailyBonusManager.myScript)
 //           DailyBonusManager.myScript.CheckDailyBonus();
 //       }
 //   }

 //   public void PopUpWelcome_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_WelComeReward.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }

 //   public void BtnAct_WelComeRewardCollect()
 //   {
 //       PlayerPrefs.SetString(Is_WelComeRewardTaken, "true");
 //       PopUpWelcome_Out();
 //       MyAdsManager.myScript.Set_Coins(1000, true);


 //       if (MyAdsManager.myScript.IsVideoAdsReady())
 //       {
 //           PopUpWelcome2_In();
 //       }
 //   }

 //   #region POPUP_WELCOME2XREWARD
 //   public void PopUpWelcome2_In()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       iTween.ScaleTo(PopUp_WelComeReward2.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn_WC2XClose.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUpWelcome2_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_WelComeReward2.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        
 //   }

 //   public void BtnAct_2WelComeRewardCollect()
 //   {
 //       MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.Welcome2x;
 //       MyAdsManager.myScript.ShowRewardVideo();
 //   }

 //   public void DoubleRewardSuccess2XWC()
 //   {
 //       PopUpWelcome2_Out();
 //       MyAdsManager.myScript.Set_Coins(1000, true);
 //   }
 //   #endregion

 //   #region POPUP_UNLOCKALLUPGRADES
 //   public void PopUpUnlockUpgrades_In()
 //   {
 //       MyDebug.Log("[PH] PopUpUnlockUpgrades_In");
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       iTween.ScaleTo(PopUp_UnlockAllUpgrades.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUpUnlockUpgrades_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_UnlockAllUpgrades.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }
 //   public void BtnAct_UnlockAllUpgrades(int _index)
 //   {
 //       BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(_index);
 //   }
 //   #endregion

 //   #region POPUP_UNLOCKALLLEVELS
 //   public void PopUpUnlockLevels_In()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       iTween.ScaleTo(PopUp_UnlockAllLevels.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUpUnlockLevels_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_UnlockAllLevels.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }
 //   public void BtnAct_UnlockAllLevels(int _index)
 //   {
 //       BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(_index);
 //   }
 //   #endregion

 //   #region POPUP_DISCOUNT_50&75
 //   public void PopUp50Disc_In()
 //   {
 //       MyDebug.Log("[PH] PopUp50Disc_In");
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       PopUp_50Disc.gameObject.SetActive(true);
 //       Text_50Disc.text = "" + MyAdsReadXML.myScript.Text_Discount50;
 //       iTween.ScaleTo(PopUp_50Disc.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn50Close.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUp50Disc_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_50Disc.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn50Close.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }
 //   public void BtnAct_50Disc(int _index)
 //   {
 //       BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(_index);
 //   }


 //   public void PopUp75Disc_In()
 //   {
 //       MyDebug.Log("[PH] PopUp75Disc_In");
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       PopUp_75Disc.gameObject.SetActive(true);
 //       Text_75Disc.text = "" + MyAdsReadXML.myScript.Text_Discount75;
 //       iTween.ScaleTo(PopUp_75Disc.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn75Close.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 2, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUp75Disc_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_75Disc.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn75Close.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }
 //   public void BtnAct_75Disc(int _index)
 //   {
 //       BtnAct_AdsRelated.CallIAPBuy(_index);
 //   }

 //   public Text Text_50offText;
 //   public Text Text_75offText;

 //   public static int _tempcount;

 //   public void Check_Discount()
 //   {
 //       if (!MyAdsManager.myScript)
 //           return;

 //       if (MyAdsUtils.Is_NetworkAvail())
 //       {
 //           if (!Utility.CheckHash(MyAdsGamePrefs.IsDiscount50Purchased) && !Utility.CheckHash(MyAdsGamePrefs.IsDiscount75Purchased))
 //           {
 //               if (MyAdsReadXML.myScript.Is_Discount75)
 //               {
 //                   _tempcount++;
 //                   if (_tempcount >= 3)
 //                   {
 //                       Text_75offText.text = MyAdsReadXML.myScript.Text_Discount75;
 //                       PopUp75Disc_In();
 //                       _tempcount = 0;
 //                   }
 //               }
 //               else
 //               if (MyAdsReadXML.myScript.Is_Discount50)
 //               {
 //                   _tempcount++;
 //                   if (_tempcount >= 3)
 //                   {
 //                       Text_50offText.text = MyAdsReadXML.myScript.Text_Discount50;
 //                       PopUp50Disc_In();
 //                       _tempcount = 0;
 //                   }
 //               }
 //               else
 //               {
 //                   //Tween_PopUp.myScript.UnlockAll_In();
 //               }
 //           }
 //       }
 //   }
 //   #endregion

 //   #region POPUP_2XLCREWARD
 //   public void PopUp2XLCReward_In()
 //   {
 //       MyDebug.Log("[PH] PopUp2XLCReward_In");
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       iTween.ScaleTo(PopUp_2XLCReward.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn_2XLCRewardClose.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUp2XLCReward_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_2XLCReward.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn_2XLCRewardClose.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }

 //   public void BtnAct_2XLCRewardCollect()
 //   {
 //       MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.DoubleReward;
 //       MyAdsManager.myScript.ShowRewardVideo();
 //   }

 //   public void VideoSuccess2XLCReward()
 //   {
 //       //MyAdsManager.myScript.Set_Coins(1000);
 //       PopUp2XLCReward_Out();
 //       //ToastMessage.showToast("Congratulations!\n2 X Welcome Reward 1000 "+MyAdsManager.myScript.RewardType+" Successfully Added!!");
 //   }
 //   #endregion

 //   #region POPUP_2XDAILYBONUS
 //   public void PopUp2XDailyBonus_In()
 //   {
 //       MyDebug.Log("[PH] PopUp2XDailyBonus_In");
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
 //       iTween.ScaleTo(PopUp_2XDailyBonus.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
 //       iTween.ScaleTo(Btn_2XDailyBonusClose.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
 //   }

 //   public void PopUp2XDailyBonus_Out()
 //   {
 //       AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
 //       iTween.ScaleTo(PopUp_2XDailyBonus.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
 //   }

 //   public void BtnAct_2XDailyBonusCollect()
 //   {
 //       MyAdsManager.myScript.RewardVideoType = MyAdsManager.RewardVideoTypes.DailyBonusDouble;
 //       MyAdsManager.myScript.ShowRewardVideo();
 //   }
 //   #endregion

}
