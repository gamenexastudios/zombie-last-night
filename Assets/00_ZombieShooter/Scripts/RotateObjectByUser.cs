﻿using UnityEngine;
using System.Collections;

public class RotateObjectByUser : MonoBehaviour 
{
	bool forMobile,touching;
	GameObject _temp,_parentToRotateByUser;

	public static RotateObjectByUser _rotatingObj;

	Quaternion startRot ;
	private float _totalCoins;
	bool canPress,canRotate;
//	void OnEnable()
//	{
//		if (_rotatingObj == null)
//			_rotatingObj = this;
//	}
//
//	void OnDisable()
//	{
//		if (_rotatingObj != null)
//			_rotatingObj = null;
//	}
  	void Start () 
	{
		_temp = new GameObject ();
//		_temp.transform.rotation = Quaternion.Euler (0, 0, 0);

//		_parentToRotateByUser = transform.GetChild (0).gameObject;
		_parentToRotateByUser = transform.gameObject;

		_temp.transform.rotation = _parentToRotateByUser.transform.rotation;
		#if !UNITY_EDITOR && !UNITY_WEBPLAYER
		forMobile = true;
		#endif
		startRot = _parentToRotateByUser.transform.rotation;
//		canPress = true;
		canRotate = true;

	}
	

	void Update () 
	{
		if (forMobile) 
		{
			var touches = Input.touches;
			//detect which mobile buttons are pressed and make decisions accordingly
			foreach (var touch in touches) 
			{							
				if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved) 
				{
					_temp.transform.Rotate (-Vector3.up, touch.deltaPosition.x *5*Time.deltaTime); 
				}
			 }
		} 
		else
		if (Input.GetMouseButton (0)) 
		{
			_temp.transform.Rotate (-Vector3.up, Input.GetAxis ("Mouse X") * 6); 
 		}
 

 		if(canRotate) 
		{
//			_parentToRotateByUser.transform.rotation = Quaternion.Slerp (_parentToRotateByUser.transform.rotation, _temp.transform.rotation, Time.deltaTime * 10);
			_parentToRotateByUser.transform.localRotation = Quaternion.Slerp (_parentToRotateByUser.transform.localRotation, _temp.transform.localRotation, Time.deltaTime * 10);
		}
		 
	}
}
