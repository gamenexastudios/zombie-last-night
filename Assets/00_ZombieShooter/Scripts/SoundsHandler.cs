﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsHandler : MonoBehaviour 
{
	public static SoundsHandler _instance;
//	public GameObject mg_SoundsObj;

	public AudioSource Sound_MenuBG;
	public AudioSource Sound_UpgradeBG;
	public AudioSource Sound_BtnClick;

	public AudioSource Sound_Theme1, Sound_Theme2, Sound_Theme3;
	public AudioSource Sound_LC, Sound_LF;

	public List<AudioSource> mlist_Sounds;
	void Awake()
	{
		DontDestroyOnLoad (this);
		if (_instance == null)
			_instance = this;
	}
	void OnEnable()
	{
		if (_instance == null)
			_instance = this;
	}
	void OnDisable()
	{
		if (_instance)
			_instance = null;
	}
	void Start () 
	{
		for(int i=0;i<this.transform.childCount;i++)
			mlist_Sounds.Add(this.transform.GetChild(i).GetComponent<AudioSource>());
	}

	public void StopAllSounds()
	{
		for (int i = 0; i < mlist_Sounds.Count; i++)
			mlist_Sounds [i].Stop ();
	}

}
