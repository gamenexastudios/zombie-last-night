﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StaticVariables : MonoBehaviour 
{

	static string _sTotalCoins = "TotalCoinsA";
	static string _sSlowMotionCount = "SlowMotionCountA";
	static string _sFreezeCount = "FreezeCountA";
	static string _sGranedeCount="GranedeCountA";

	static string _sSelectedWeapon="SelectedGun_AssaultA";
	static string _sSelectedLevel="SelectedLevelA";
	static string _sUnlockedLevels="UnlockedLevelsA";
	public static string strUnlockedWeaponsRef = "11111111111111111111";

	public static string _sSceneToLoad = "0_Menu";

	public static bool mb_ShowLevelsel=false;
	public static int mi_TotalLevels=20;

	public static int [] snowl=new int[]{1,5,7,9};

 	static string str_SoundStatus = "SC_Sound";
	public static int _iEnablePageID=100;

	public static int[] _iGoldUnlockUpgrades = new int[] {4,6,9,12,17,19};

	public static int _iHelpStatus {
		get{
			return PlayerPrefs.GetInt ("Help", 0);
		}
		set{
			PlayerPrefs.SetInt ("Help", value);
		}
	}

	public static int iSoundStatus {
		get{
			return PlayerPrefs.GetInt (str_SoundStatus, 1);
		}
		set{
			PlayerPrefs.SetInt (str_SoundStatus, value);
		}
	}

	public static int _iTotalCoins{
		get{
			return PlayerPrefs.GetInt (_sTotalCoins, 0);
		}
		set{
			PlayerPrefs.SetInt (_sTotalCoins, value);
		}
	}

	public static int _iTotalGold{
		get{
			return PlayerPrefs.GetInt ("TotalGold", 0);
		}
		set{
			PlayerPrefs.SetInt ("TotalGold", value);
		}
	}

	public static int _iSelectedWeapon{
		get{
			return PlayerPrefs.GetInt (_sSelectedWeapon, 1);
		}
		set{
			PlayerPrefs.SetInt (_sSelectedWeapon, value);
		}
	}

	public static int _iCurrentLevel{
		get{
			return PlayerPrefs.GetInt (_sSelectedLevel, 1);
		}
		set{
			PlayerPrefs.SetInt (_sSelectedLevel, value);
		}
	}

	public static int m_iUnlockedLevels{
		get{
			return PlayerPrefs.GetInt (_sUnlockedLevels,1);
		}
		set{
			PlayerPrefs.SetInt (_sUnlockedLevels, value);
		}
	}

	public static string strUnlockedWeapons {
		get{
			return PlayerPrefs.GetString ("strUnlockedWeapons", "100000000000000000000");
		}
		set{
			PlayerPrefs.SetString ("strUnlockedWeapons", value);
		}
	}

	public static int _iSlowMotionCount{
		get{
			return PlayerPrefs.GetInt (_sSlowMotionCount, 1);
		}
		set{
			PlayerPrefs.SetInt (_sSlowMotionCount, value);
		}
	}
	public static int _iFreezeCount{
		get{
			return PlayerPrefs.GetInt (_sFreezeCount, 1);
		}
		set{
			PlayerPrefs.SetInt (_sFreezeCount, value);
		}
	}

	public static int _iGranedeCount{
		get{
			return PlayerPrefs.GetInt (_sGranedeCount, 3);
		}
		set{
			PlayerPrefs.SetInt (_sGranedeCount, value);
		}
	}

	public static int _iHealthCount{
		get{
			return PlayerPrefs.GetInt ("HealthCount", 3);
		}
		set{
			PlayerPrefs.SetInt ("HealthCount", value);
		}
	}

	public static int _iRank{
		get{
			return PlayerPrefs.GetInt ("Rank", 10);
		}
		set{
			PlayerPrefs.SetInt ("Rank", value);
		}
	}
	public enum e_GameMode
	{
		Career,
		Survival,
		Bounty
	}
	public static e_GameMode m_GameMode;
}
