﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SurvivalMode : MonoBehaviour 
{
	public static SurvivalMode _instance;
	void Awake()
	{
		_instance = this;
	}
	public int _iZombieCount;
	public List<ZombieBehavior> mlist_Zombies;
	[HideInInspector]
	public int _iKilledCount;
    Vector3 playerPos;

	public void Set_SurvivalModeProperties (Vector3 playerPoss) 
	{
		for(int i=0;i<_iZombieCount;i++)
		{
            playerPos = playerPoss;
			RandomNavmeshLocation (playerPoss);
			int _rnd=Random.Range(0,InGameUIHandler._instance.mlist_ZombiePrefabs.Count);
			GameObject _gZombie = Instantiate (InGameUIHandler._instance.mlist_ZombiePrefabs[_rnd], RandomNavmeshLocation(playerPoss), Quaternion.identity)as GameObject;
//			GameObject _obj=Instantiate (Resources.Load ("Zombie_" + Random.Range (1, 8)), RandomNavmeshLocation(), Quaternion.identity)as GameObject;
			mlist_Zombies.Add(_gZombie.GetComponent<ZombieBehavior>());
			mlist_Zombies [i].mb_RespondinStart = true;
		}
	}
	public void Check_ZombieCount()
	{
		_iKilledCount++;
		InGameUIHandler._instance.Text_KilledCount.text = "KILLED COUNT : " + _iKilledCount;
		if(mlist_Zombies.Count<=0)
		{
			Set_SurvivalModeProperties (playerPos);
		}
	}
	public Vector3 RandomNavmeshLocation(Vector3 center)
	{
		Vector3 randomDirection =Random.insideUnitSphere*35;
		randomDirection += center*1.08f;
		NavMeshHit hit;
		Vector3 finalPosition = Vector3.zero;
		if (NavMesh.SamplePosition(randomDirection, out hit, 30, 1)) 
		{
			finalPosition = hit.position;            
		}
		return finalPosition;
	}
	/*
	public static void shuffleList<T>(List<T> arr) 
	{
		for (int i = arr.Count - 1; i > 0; i--) 
		{
			int r = Random.Range(0, i);
			T tmp = arr[i];
			arr[i] = arr[r];
			arr[r] = tmp;
		}
	}
	List<int> randomNumber = new List<int> ();
	List <int> spawnArr;
	void Fill_List ()
	{
		for (int i = 0; i < spawnArr.Count; i++) {
			randomNumber.Add (i);
		}
	}
	int RandomNumberChooser ()
	{
		if (randomNumber.Count == 0) {
			Fill_List ();
			RandomNumberChooser ();
		}
		var index = Random.Range (0, randomNumber.Count);
		var value = randomNumber [index];
		randomNumber.RemoveAt (index);
		return value;
	}*/
}
