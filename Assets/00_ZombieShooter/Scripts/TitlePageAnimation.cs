﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitlePageAnimation : MonoBehaviour 
{
	public GameObject mg_Title,mg_TitleFill, mg_BtnContinue;
	void OnEnable () 
	{
		iTween.ScaleFrom (mg_Title.gameObject, iTween.Hash ("x", 0, "y", 0,"delay",0.5f ,"time", 0.75f, "easetype", iTween.EaseType.easeOutQuint));
		iTween.ScaleFrom (mg_BtnContinue.gameObject, iTween.Hash ("x", 0, "y", 0,"delay",0.75f ,"time", 0.75f, "easetype", iTween.EaseType.easeOutElastic));
//		iTween.ScaleFrom (mg_TitleFill.gameObject, iTween.Hash ("x", 2, "y", 2,"delay",1f ,"time", 0.75f, "easetype", iTween.EaseType.easeOutElastic));
		iTween.MoveTo(mg_TitleFill.gameObject, iTween.Hash ("z", 0, "delay",1f ,"time", 0.75f,"islocal",true,"easetype", iTween.EaseType.easeOutElastic));
		Invoke ("SoundEffect", 1f);
//		iTween.ValueTo (mg_TitleFill.gameObject, iTween.Hash ("from", 0, "to", 1, "Time", 0.5f, "easeType", iTween.EaseType.linear, "delay", 0.8, "onupdate", "ChangeVal","OnupdateTarget",this.gameObject));
	}
	void SoundEffect()
	{
		if (SoundsHandler._instance)
			SoundsHandler._instance.Sound_BtnClick.Play ();
	}
//	void ShakeCam()
//	{
//		Debug.Log ("---- ");
//		iTween.ShakePosition (Camera.main.gameObject,iTween.Hash("x",0.2f,"time",0.25f));
//	}
	void ChangeVal(float newVal)
	{
//		Debug.Log ("---NewValue");
		mg_TitleFill.gameObject.GetComponent<Image>().fillAmount = newVal;
	}
}
