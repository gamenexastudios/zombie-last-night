﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateCoins : MonoBehaviour 
{
	public Text Text_Coins;
	public bool _isCash=true;
	void Start () {
		Text_Coins = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_isCash)
			Text_Coins.text = "" + StaticVariables._iTotalCoins;
		else
			Text_Coins.text = "" + StaticVariables._iTotalGold;
	}
}
