﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class UpgradeManager : MonoBehaviour 
{

	public static UpgradeManager _instance;

	public int mi_UpgradeID;

	public GameObject _gMainObj;
	public List<GameObject> _gUpgradeObjs;
	[Header ("===============Upgrade_Details==============")]
	public Text Text_UpgradeName;
	public Image im_Rifle;
	public Image im_FireRate, im_Damage,im_Accuracy, im_Statbility;
	public Image im_BuyImageStatus;
	public Sprite spr_Gold, spr_Cash;
	public string[] ms_UpgradeNames;

	public float [] mf_Rifle,mf_Accuracy;
	public float [] mf_FireRate,mf_Damage,mf_Stability;
	public Image[] im_gunStatus;
	public Sprite spr_StatusOn, spr_StatusOff;

	[Header("============Button Visibility===============")]
	public GameObject mg_BtnPlay;
	public GameObject mg_BtnBuy,mg_BtnFBShare;
	public Text Text_UpgradeCost;
	public GameObject mg_BtnNext, mg_BtnPrev;

	public GameObject[] mg_BtnUnlockAllUpgrades;

	[Header("============== Gun Cost =======================")]
	public int[] mi_UpgradeCost;
	[Header("============== UI Obj's ================")]
	public GameObject[] mg_UIObjs;
	public int mi_PageID;

	public static bool _isFlamerSelected;
	[Header("============== Extra =======================")]
	public GameObject mg_FlammerUI;
	public GameObject mg_DoublePistolUI;
	public GameObject mg_UnlockedGunUI;
	public GameObject mg_WarningPopup;


    
    public ParticleSystem uiParticle;

    public int GunSelectionAd;
    void Awake()
	{
		_instance = this;
		for (int i=0;i<_gMainObj.transform.childCount;i++)
		{
			_gUpgradeObjs.Add (_gMainObj.transform.GetChild (i).gameObject);
		}
	}
	void Start()
	{
		Debug.Log ("---- Selected Gun::::" + StaticVariables._iSelectedWeapon);
		mi_UpgradeID = StaticVariables._iSelectedWeapon;
		//		StartCoroutine(Enable_UIObj (0)) ;
//		StaticVariables._iCurrentLevel = 5;
		if(StaticVariables._iCurrentLevel==5 && !_isFlamerSelected)
		{
			Debug.Log ("---- Show Flammer Appriciation Here");
//			StaticVariables._iSelectedWeapon = 7;
			mi_UpgradeID = 7;
//			Show_Flammer ();
		}

		StartCoroutine(Dispaly_Gun (0.1f)) ;
		Check_BtnVisibility ();
		Check_GunWarns ();

		//UnlockAllUpgrades ();  //Only for Testing Purpose

	}
	void OnEnable()
	{
		if (_instance == null)
			_instance = this;

		if (SoundsHandler._instance) {
			SoundsHandler._instance.StopAllSounds ();
			SoundsHandler._instance.Sound_UpgradeBG.Play ();
		}
	}
	void Disable () 
	{
		if (_instance)
			_instance = null;
	}

	void Show_Flammer()
	{
		Debug.Log ("---- Disabel Btns");
		mg_BtnBuy.SetActive (false);
		mg_FlammerUI.SetActive (true);
		for (int i = 0; i < im_gunStatus.Length; i++)
			im_gunStatus [i].transform.GetChild (0).GetComponent<Button> ().interactable = false;
	}
		
	public void Btn_GunSelect(int _num)
	{
		if (SoundsHandler._instance)
			SoundsHandler._instance.Sound_BtnClick.Play ();
		
		foreach (GameObject Go in _gUpgradeObjs)
			Go.SetActive (false);
		SelectGun (_num);
	}

	public void OnBtnClick(string _str)
	{
		if (SoundsHandler._instance)
			SoundsHandler._instance.Sound_BtnClick.Play ();
		switch(_str)
		{
			case "Btn_Play":
#if !AdSetup_OFF

                if (GunSelectionAd == 1)
					GoogleAdMobsManager.instance.DisplayInterstitial_Normal();
#endif

				char[] _tempUpgrades = StaticVariables.strUnlockedWeapons.ToCharArray ();
			if (_tempUpgrades [mi_UpgradeID - 1] == '1') 
			{
				LoadLevel ();
			} 
			else
			{
				Debug.Log ("--- This is Locked :::: Call INAPP");
				OnBtnClick ("Btn_Buy");
			}
			break;

		case "Btn_NextUpgrade":
			if(mi_UpgradeID<_gUpgradeObjs.Count)
			{
				mi_UpgradeID++;
				SelectGun (mi_UpgradeID);
			}
			break;

		case "Btn_PrevUpgrade":
			if(mi_UpgradeID!=0)
			{
				mi_UpgradeID--;
				SelectGun (mi_UpgradeID);
			}
			break;

		case "Btn_Buy":
			PurchaseCheck ();
			break;
		case "Btn_FBShare":
			OnYoutube ();
			break;

		case "Btn_UpBack":
			Debug.Log ("BACK");
			MenuHandler._instance.Call_Upgrade (false);
			MenuHandler._instance.EnableUIPage (5);
			break;

		case"Btn_Store":
			Disable_Upgrade ();
			StartCoroutine (Enable_UIObj (2));
			OnBtnClick ("Btn_Store");
			break;

		case"Btn_FlammerSelect":
			mg_FlammerUI.SetActive (false);
			LoadLevel ();
			break;

		case"Btn_DblePistolCliam":
			mg_DoublePistolUI.SetActive (false);
			SelectGun (2);
			OnYoutube ();
			break;
		case"Btn_UnlockedSuccess":
			mg_UnlockedGunUI.SetActive (false);
//			SelectGun (2);
//			OnYoutube ();
			break;
		}
	}

	void Disable_Upgrade()
	{
		for (int i = 0; i < _gUpgradeObjs.Count; i++)
			_gUpgradeObjs [i].SetActive (false);
	}
		
	IEnumerator DisableObj(float _delay, GameObject _Obj)
	{
		yield return new WaitForSeconds (_delay);
		_Obj.SetActive (false);
	}

	public void SelectGun(int ID)
	{
        //FirebaseEvents.instance.LogFirebaseEvent("Funnel_GunSelection", "Gun: " + ID, "Gun Selection Button");
        mi_UpgradeID =ID;
		StartCoroutine(Dispaly_Gun (0.15f)) ;
	}

	public IEnumerator Dispaly_Gun(float _delay)
	{
		yield return new WaitForSeconds (_delay);
		for (int i = 0; i < _gUpgradeObjs.Count; i++)
		{
			_gUpgradeObjs [i].SetActive (false);
			im_gunStatus [i].sprite = spr_StatusOff;
//			_gUpgradeObjs [i].transform.localScale = Vector3.zero;
		}
		_gUpgradeObjs [mi_UpgradeID - 1].SetActive (true);
		im_gunStatus [mi_UpgradeID - 1].sprite = spr_StatusOn;
//		iTween.ScaleTo (_gUpgradeObjs [mi_UpgradeID - 1].gameObject, iTween.Hash ("x", 0.7,"y",0.7,"z",0.7, "time", 0.5f, "easetype", iTween.EaseType.spring));
		Assign_GunDetails ();
		Check_GunAvailability ();

	}
	void Check_GunWarns()
	{
		if(StaticVariables.m_iUnlockedLevels==3)
		{
			char[] _tempUpgrades = StaticVariables.strUnlockedWeapons.ToCharArray ();
			if(_tempUpgrades [1] != '1')
			{
				mg_DoublePistolUI.SetActive (true);
				GameObject Go = mg_DoublePistolUI.transform.GetChild (0).gameObject;
				iTween.ScaleFrom (Go.gameObject, iTween.Hash ("Scale", Vector3.zero, "time", 0.5f, "easetype", iTween.EaseType.easeOutBack));
			}
		}
		if(StaticVariables.m_iUnlockedLevels==7)
		{
			Debug.Log ("--- Warn");

			char[] _tempUpgrades = StaticVariables.strUnlockedWeapons.ToCharArray ();
			if(_tempUpgrades [2] != '1')//|| _tempUpgrades[3]!=1)
			{
				mg_WarningPopup.SetActive (true);
				GameObject Go = mg_WarningPopup.transform.GetChild (0).gameObject;
				iTween.ScaleFrom (Go.gameObject, iTween.Hash ("Scale", Vector3.zero, "time", 0.5f, "easetype", iTween.EaseType.easeOutBack));
			}
		}
	}

	public void Close_UpgradeWarn()
	{
		mg_WarningPopup.SetActive (false);
		SelectGun (3);
	}

	void Assign_GunDetails()
	{
//		Text_UpgradeName.text = "" + ms_UpgradeNames [mi_UpgradeID - 1];
		Text_UpgradeName.SetLocalizedTextForTag (ms_UpgradeNames [mi_UpgradeID - 1]);
//		Text_UpgradeName.SetLocalizedTextForTag
		im_Rifle.fillAmount=mf_Rifle[mi_UpgradeID - 1];
		im_Accuracy.fillAmount =mf_Accuracy[mi_UpgradeID - 1];
		im_FireRate.fillAmount = mf_FireRate[mi_UpgradeID - 1];
		im_Damage.fillAmount = mf_Damage [mi_UpgradeID - 1];
		im_Statbility.fillAmount =  mf_Stability [mi_UpgradeID - 1];
		if(StaticVariables._iGoldUnlockUpgrades.Contains(mi_UpgradeID))
		{
			im_BuyImageStatus.sprite = spr_Gold;
		}
		else
		{
			im_BuyImageStatus.sprite = spr_Cash;
		}
	}

	void Check_GunAvailability()
	{
		char[] _tempUpgrades = StaticVariables.strUnlockedWeapons.ToCharArray ();
		if (_tempUpgrades [mi_UpgradeID - 1] == '1')
		{
			mg_BtnFBShare.SetActive (false);
			mg_BtnPlay.SetActive (true);
			mg_BtnBuy.SetActive (false);
		} 
		else
		{
//			if (mi_UpgradeID == 2) 
//			{
//				mg_BtnPlay.SetActive (false);
//				mg_BtnFBShare.SetActive (true);
//				mg_BtnBuy.SetActive (false);
//			} 
//			else 
			{
				mg_BtnFBShare.SetActive (false);
				mg_BtnBuy.SetActive (true);
				mg_BtnPlay.SetActive (false);
				Text_UpgradeCost.text = "" + mi_UpgradeCost [mi_UpgradeID-1];
			}
		}
		if (StaticVariables._iCurrentLevel == 5 && !_isFlamerSelected)
			Show_Flammer ();
	}

	public void LoadLevel()
	{
		Debug.Log ("---- To Load Level :::: " + StaticVariables._iCurrentLevel);
		StaticVariables._iSelectedWeapon = mi_UpgradeID;
		switch(StaticVariables._iCurrentLevel)
		{
		case 1: case 4: case 6 : case 9: case 12:case 16:case 19: case 24: case 28: case 29: case 30: case 37: case 38:
			StaticVariables._sSceneToLoad="Theme_1";
			SceneManager.LoadScene ("LoadingScene");
			break;
		case 5: case 8: case 11:case 14: case 15: case 18: case 20: case 25: case 26: case 27: case 31: case 32: case 33: case 34: case 35:
            case 36:
			StaticVariables._sSceneToLoad="Theme_2AA";
			SceneManager.LoadScene ("LoadingScene");
			break;
		case 2: case 3: case 7: case 10:case 13: case 17: case 23:
			StaticVariables._sSceneToLoad="Theme_3BNew";
			SceneManager.LoadScene ("LoadingScene");
			break;
        case 21: case 22:
            StaticVariables._sSceneToLoad = "Theme_1";
            SceneManager.LoadScene("LoadingScene");
            break;
		}
	}
		
	public IEnumerator Enable_UIObj (int ID)
	{
		yield return new WaitForSeconds (0.1f);
		for (int i = 0; i < mg_UIObjs.Length; i++)
		{
			mg_UIObjs [i].GetComponent<PageAnimHandler> ().Call_TweenOut ();
			mg_UIObjs [i].gameObject.transform.localScale = Vector3.zero;
		}
		yield return new WaitForSeconds (0.15f);
		mg_UIObjs [ID].gameObject.transform.localScale = Vector3.one;
		mg_UIObjs [ID].GetComponent<PageAnimHandler> ().Call_TweenIn ();
	}
		
	void PurchaseCheck()
	{
		if(StaticVariables._iGoldUnlockUpgrades.Contains(mi_UpgradeID))
		{
			if (mi_UpgradeCost [mi_UpgradeID-1] <= StaticVariables._iTotalGold)
			{
				//Debug.Log ("---- Purchase  Success");
				StaticVariables._iTotalGold -= mi_UpgradeCost [mi_UpgradeID-1];
				Unlock_Upgrade ();
                StartCoroutine(PlayParticleEffectOnUi());
			}
			else
			{
				//Debug.Log ("----  Call Inapp Purchase :::: Not Enough GOLD");
                BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(2);
            }
		}
		else
		{
			if (mi_UpgradeCost [mi_UpgradeID-1] <= StaticVariables._iTotalCoins)
			{
				//Debug.Log ("---- Purchase  Success");
				StaticVariables._iTotalCoins -= mi_UpgradeCost [mi_UpgradeID-1];
				Unlock_Upgrade (); 
                StartCoroutine(PlayParticleEffectOnUi());
            }
			else
			{
				//Debug.Log ("----  Call Inapp Purchase :::: Not Enough CASH");
                BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(2);
			}
		}
		Check_BtnVisibility ();
	}

    IEnumerator PlayParticleEffectOnUi()
    {
        yield return new WaitForSeconds(2f);
        uiParticle.Play();
    }
	void Unlock_Upgrade()
	{
		char[] TempChar = StaticVariables.strUnlockedWeapons.ToCharArray ();
		TempChar [mi_UpgradeID-1] = '1';
		StaticVariables.strUnlockedWeapons = "";
		foreach (char ch in TempChar)
		{
			StaticVariables.strUnlockedWeapons += ch.ToString ();
		}
		mg_UnlockedGunUI.SetActive (true);
		GameObject Go = mg_UnlockedGunUI.transform.GetChild (0).gameObject;
		iTween.ScaleFrom (Go.gameObject, iTween.Hash ("Scale", Vector3.zero, "time", 0.5f, "easetype", iTween.EaseType.easeOutBack));
		Check_GunAvailability ();
	}

	void OnYoutube()
	{
		char[] _tempUpgrades = StaticVariables.strUnlockedWeapons.ToCharArray ();
		_tempUpgrades [1] = '1';
		StaticVariables.strUnlockedWeapons = "";
		foreach (char ch in _tempUpgrades)
			StaticVariables.strUnlockedWeapons += ch.ToString ();
		Check_GunAvailability();
	}
		
	public static void UnlockAllUpgrades()
	{
		Debug.Log ("---- Unlocked Upgrades");
		StaticVariables.strUnlockedWeapons = StaticVariables.strUnlockedWeaponsRef;
		_instance.Check_GunAvailability ();
		_instance.Check_BtnVisibility ();
	}
	public GameObject mg_BtnStoreUnlockUp;
	public GameObject mg_BtnUnlockAll;
	public GameObject mg_BtnStoreUnlockLvl;

	public void Check_BtnVisibility()
	{
		if(StaticVariables.strUnlockedWeapons == StaticVariables.strUnlockedWeaponsRef)
		{
			mg_BtnUnlockAllUpgrades [0].SetActive (false);
			mg_BtnStoreUnlockUp.SetActive (false);
		}
		if(StaticVariables.m_iUnlockedLevels == StaticVariables.mi_TotalLevels && StaticVariables.strUnlockedWeapons == StaticVariables.strUnlockedWeaponsRef)
		{
			mg_BtnUnlockAll.SetActive (false);
		}
		if(StaticVariables.m_iUnlockedLevels == StaticVariables.mi_TotalLevels)
		{
			mg_BtnStoreUnlockLvl.SetActive (false);
		}
	}
}
