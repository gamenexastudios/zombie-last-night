﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DailyBonusManager : MonoBehaviour 
{
    public GameObject _closeButton;
	public static DailyBonusManager myScript;

	public GameObject DailyBonusObj;
	public float DelayForShowBonus;
	public GameObject[] BtnCollect;

	[HideInInspector]
	public int PreviousBonus;

	DateTime oldDate;
	DateTime currentDate;
	TimeSpan Total_Diff_Time;

	int Diff_Days;
	long temp;

	[HideInInspector]
	public int BonusAt_PreviousDay;
	[HideInInspector]
	public int BonusAt_Day;
	public static string LastDayTime = "LastDayTimeA";
	public static string BonusTaken = "BonusTakenB";
	public static string PreviousBonusC="PreviousBonusCC";

	public int[] Rewards1;
	public GameObject[] TomorrowBanner;
	public GameObject[] TickMark;
	public Sprite[] BgSprite; 
	public Text[] Text_Bonus;

	public RectTransform Btn_ClaimBtn;

    //*********************************GAME NEXA CODE********************************
    public bool _launchAdd;
    public GameObject _2xRewardPannel;
    //END

	void Awake()
	{
		myScript=this;
		if(PlayerPrefs.HasKey(LastDayTime)==false)
		{
			PlayerPrefs.SetString(LastDayTime, System.DateTime.Now.ToBinary().ToString());
		}

		if(!PlayerPrefs.HasKey(BonusTaken))
		{
			PlayerPrefs.SetString(BonusTaken,"false");   
		}
        this.gameObject.SetActive(false);
        CheckDailyBonus(); // TEST

        //*********************************GAME NEXA CODE********************************
        _launchAdd = true;
        //END
    }

	public void Set_BonuseText(int _day)
	{
		for (int i = 0; i < Text_Bonus.Length; i++) 
		{
			if (_day == i) 
			{				
				//Text_Bonus [i].gameObject.transform.parent.gameObject.GetComponent<Image> ().sprite = BgSprite [0];
			}

			if (i == (_day + 1) && (_day+1)<=Text_Bonus.Length) 
			{
				TomorrowBanner [i].gameObject.SetActive (true);
				//Text_Bonus [i].alignment = TextAnchor.MiddleLeft;
			}
			else 
			{
				TomorrowBanner [i].gameObject.SetActive (false);
			}

			if (_day >= 1) 
			{
				if (i <= _day - 1) 
				{
					TickMark [_day - 1].gameObject.SetActive (true);
					//Text_Bonus [i].gameObject.transform.parent.gameObject.GetComponent<Image> ().sprite = BgSprite [0];
				}
				else 
				{
					TickMark [i].gameObject.SetActive (false);
				}
			}
			else 
			{
				TickMark [i].gameObject.SetActive (false);
			}
			Text_Bonus[i].text=""+Rewards1[i]+" Cash";
		}

		print ("DAY : " + _day);
	}

	public bool CheckDailyBonus()
	{
		currentDate = System.DateTime.Now;
		temp = Convert.ToInt64(PlayerPrefs.GetString(LastDayTime));
		oldDate = DateTime.FromBinary(temp);
		Total_Diff_Time = 	currentDate.Subtract(oldDate);
		Diff_Days=Total_Diff_Time.Days;
        print("Total Diff Days : " + Diff_Days);

        if (!PlayerPrefs.HasKey(PreviousBonusC))
		{			
			PlayerPrefs.SetInt(PreviousBonusC,0);
			BonusAt_Day=1;
			PlayerPrefs.SetInt(PreviousBonusC,BonusAt_Day);
            Invoke("ShowBonus",DelayForShowBonus);
            print("Bonus AT FirstTime");

            return true;
        }
		else
		{
			BonusAt_PreviousDay=PlayerPrefs.GetInt(PreviousBonusC);
			if(Diff_Days>=1)
			{
				Diff_Days=1;
				BonusAt_Day=BonusAt_PreviousDay+1;
				PlayerPrefs.SetInt(PreviousBonusC,BonusAt_Day);
				PlayerPrefs.SetString(BonusTaken,"false");
                Invoke("ShowBonus",DelayForShowBonus);
                print("Bonus AT Normal Day");
                return true;      
            }
            else
			{
				if(PlayerPrefs.GetString(BonusTaken)=="false")
				{
					Diff_Days=1;
					BonusAt_Day=BonusAt_PreviousDay;
					PlayerPrefs.SetInt(PreviousBonusC,BonusAt_Day);
					PlayerPrefs.SetString(BonusTaken,"false");
                    Invoke("ShowBonus",DelayForShowBonus);
                    print("Bonus At Missed Day");
                    return true;
                }
				else
				{
                    print("Bonus Already Taken");
                    return false;
                }
			}
		}

        print("Bonus Day : " + BonusAt_Day);
    }

    public GameObject PopUp_AlphaBG;
    public Text Text_2xText;
    void ShowBonus()
	{
        //*********************************GAME NEXA CODE********************************
        _launchAdd = false;
        //END

        DailyBonusObj.gameObject.SetActive (true); // Showing DailyBonus Page Here

        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        iTween.ScaleTo(DailyBonusObj.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));

        iTween.ScaleTo(Btn_ClaimBtn.gameObject, iTween.Hash("x", 1.1f,"y",1.1f, "time",0.2f, "islocal",true,"easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
		if (BonusAt_Day >= BtnCollect.Length) 
		{
			//BtnCollect [BtnCollect.Length - 1].gameObject.GetComponent<Button> ().interactable = true;
			Set_BonuseText (BtnCollect.Length - 1);

			//BtnCollect [BonusAt_Day - 1].gameObject.SetActive (false);
		}
        else
        {
			//BtnCollect [BonusAt_Day - 1].gameObject.GetComponent<Button> ().interactable = true;
			//BtnCollect [BonusAt_Day - 1].gameObject.SetActive (true);
			Set_BonuseText (BonusAt_Day - 1);
		}
    }
		
	public void CollecctBonus()
	{
        Invoke("ActivateCloseButton", 4.0f);
		PlayerPrefs.SetString (BonusTaken, "true");

		PlayerPrefs.SetString (LastDayTime, System.DateTime.Now.ToBinary ().ToString ());

		if (BonusAt_Day >= Rewards1.Length) 
		{
			//MyAdsManager.myScript.Set_Coins (Rewards1 [Rewards1.Length - 1]);
#if !AdSetup_OFF
            GoogleAdMobsManager.instance._rewardType = rewardType.coins;
#endif
            // GoogleAdMobsManager.instance.DisplayRewardVideo(Rewards1[Rewards1.Length - 1]);
             StaticVariables._iTotalCoins += Rewards1[Rewards1.Length - 1];   //Newly added
            _2xRewardPannel.SetActive(true);  //Newly added
            // GoogleAdMobsManager.instance.RequestRewardVideo();
        }

        else
		{
			//MyAdsManager.myScript.Set_Coins(Rewards1[BonusAt_Day - 1]);
#if !AdSetup_OFF
            GoogleAdMobsManager.instance._rewardType = rewardType.coins;
#endif
            // GoogleAdMobsManager.instance.DisplayRewardVideo(Rewards1[BonusAt_Day - 1]);
            StaticVariables._iTotalCoins += Rewards1[BonusAt_Day - 1];   //Newly added
            _2xRewardPannel.SetActive(true);  //Newly added
            // GoogleAdMobsManager.instance.RequestRewardVideo();
        }

        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(DailyBonusObj.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));

        //DailyBonusObj.gameObject.SetActive(false);

        //here I am Enabling Double Reward Page
        //MenuHandler.Instance.MenuObj();

        //MenuAnimationHandler.Instance.PushMenuAnimations("dailyrewardsdouble");
        //Text_2xText.text = "2" +" "+"x"+" "+ Rewards1 [BonusAt_Day- 1] +" "+"=" +" "+ 2 * Rewards1 [BonusAt_Day - 1];

        //if (MyAdsManager.myScript.IsVideoAdsReady())
        //{
        //    PopUpHandler.myScript.PopUp2XDailyBonus_In();
        //}

        //DailyBonusObj.gameObject.SetActive(false);

        //if (MyAdsManager.myScript != null)
        //{
        //    ToastMessage.showToast("Congratulations you Got Daily Reward of "+Rewards1 [BonusAt_Day - 1]+" "+MyAdsManager.myScript.RewardType+"!");
        //}

        
    }

    public void VideoSuccess2XDailyBonus()
    {
        //MyAdsManager.myScript.Set_Coins(Rewards1[BonusAt_Day - 1]);
        //PopUpHandler.myScript.PopUp2XDailyBonus_Out();
        //ToastMessage.showToast("Congratulations!\n2 X Daily Bonus "+ Rewards1[BonusAt_Day - 1]+" "+ MyAdsManager.myScript.RewardType + " Successfully Added!!");    

        if (BonusAt_Day >= Rewards1.Length)
		{
#if !AdSetup_OFF
            GoogleAdMobsManager.instance.DisplayRewardVideo(Rewards1[Rewards1.Length - 1]);
#endif
            this.gameObject.SetActive(false);
        }
        else
        {
			#if !AdSetup_OFF
            GoogleAdMobsManager.instance.DisplayRewardVideo(Rewards1[BonusAt_Day - 1]);
#endif
            this.gameObject.SetActive(false);
        }
    }

    public void CloseDailyRewardWindow()
    {
        DailyBonusObj.SetActive(false);

    }

    public void ActivateCloseButton()
    {
        _closeButton.SetActive(true);
    }
}
