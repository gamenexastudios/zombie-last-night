﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySystem;
using UnityEngine.UI;

public class MyGamePrefs : MonoBehaviour 
{
	// PLAYERPREFS STRINGS
	public static string Is_Rated = "Is_RatedA";

	public static string Is_Shared = "Is_SharedB";

	public static string Is_RewardShared = "Is_RewardSharedC";

	public static string TotalCoins = "TotalCoinsB";

    public static string CurrentLevel = "CurrentlevelinGame";

	// ONLY STATIC VARIABLES
	public static string LoadingScene="Loading";
}
