﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySystem;
using UnityEngine.UI;

public class ProjectailMotion : MonoBehaviour 
{
	public static ProjectailMotion myScript;

	public GameObject TargetObj;
	public Vector3 Target;
	public GameObject TargetPosObj;
	private Transform myTransform;

	public float firingAngle = 45.0f;
	public float gravity = 9.8f;

	void Awake()
	{
		myScript=this;
	}

	void Start () 
	{
		StartProjectail ();
	}

	public void StartProjectail()
	{
		Invoke("CheckDestination",0f);
	}

	public void CheckDestination()
	{
		myTransform = TargetObj.transform;
		StartCoroutine(SimulateProjectile());
	}

	private float DistNew;
	IEnumerator SimulateProjectile()
	{
		// Short delay added before Projectile is thrown
		yield return new WaitForSeconds(0f);

		// Move projectile to the position of throwing object + add some offset if needed.
		TargetObj.transform.position = myTransform.position + new Vector3(0, 0.0f, 0);
		//		print("My Transform : "+myTransform.transform.position);

		// Calculate distance to target
		float target_Distance=0;
		if(TargetPosObj != null)
		{
			Target = TargetPosObj.transform.position;
		}

		target_Distance = Vector3.Distance(TargetObj.transform.position, Target);

		DistNew=target_Distance;

		// Calculate the velocity needed to throw the object to the target at specified angle.
		float projectile_Velocity = DistNew / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

		// Extract the X  Y componenent of the velocity
		float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
		float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

		// Calculate flight time.
		float flightDuration = DistNew / Vx;

		// Rotate projectile to face the target.
		TargetObj.transform.rotation = Quaternion.LookRotation(Target - TargetObj.transform.position);

		float elapse_time = 0;

		while (elapse_time < flightDuration)
		{
			TargetObj.transform.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

			elapse_time += Time.deltaTime;

			if(elapse_time>=flightDuration)
			{
				//RS Reached Target Position and Here we are Instantiating Blast
			}

			yield return null;
		}
	}  
}
