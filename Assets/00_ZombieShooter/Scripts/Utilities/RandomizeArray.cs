﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySystem;
using UnityEngine.UI; 

public class RandomizeArray : MonoBehaviour 
{
	public static RandomizeArray myScript;

	public int[] ar=new int[]
	{
		0,1,2,3,4
	};

	void Awake()
	{
		myScript=this;
		RandomArray_Int (ar);
	}

	public static int[] RandomArray_Int(int[] _SourceArray)
	{
		for (int t = 0; t <_SourceArray.Length; t++)
		{
			if(t < _SourceArray.Length)
			{
				int tmp = _SourceArray [t];
				int r = Random.Range (t, _SourceArray.Length);
				_SourceArray [t] = _SourceArray [r];
				_SourceArray [r] = tmp;

			}
		}

		return _SourceArray;
//		print ("Array : " + _SourceArray [t]);
	}
}
