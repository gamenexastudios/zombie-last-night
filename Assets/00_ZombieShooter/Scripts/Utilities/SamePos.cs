﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySystem;
using UnityEngine.UI;

public class SamePos : MonoBehaviour 
{
	public static SamePos myScript;
	public GameObject TargetObj;

	void Awake()
	{
		myScript=this;
	}

	void Start () 
	{
		
	}

	void Update () 
	{
		this.transform.position = new Vector3 (TargetObj.transform.position.x,TargetObj.transform.position.y,this.transform.position.z);
	}
}
