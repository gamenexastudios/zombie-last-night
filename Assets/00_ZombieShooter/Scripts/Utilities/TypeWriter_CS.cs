﻿using UnityEngine;
using MySystem;
using System.Collections;
using UnityEngine.UI;

public class TypeWriter_CS : MonoBehaviour 
{		
	public static TypeWriter_CS myScript;
	public float LetterToLetterDelay;

	public string Word;
	private string CurrentWord;

	public Text Text_Show;

	void Awake () 
	{
		myScript=this;
	}
	
	void Start () 
	{
		Invoke("StartTyping",2f);
	}

	public void StartTyping()
	{
		StartCoroutine("TypeWriterEffect",Word);
	}

	public IEnumerator TypeWriterEffect(string TextToType)
	{
		yield return new WaitForSeconds(LetterToLetterDelay);
		foreach(char Textt in  Word.ToCharArray())
		{
			if (Word != TextToType) break;
			CurrentWord += Textt;
			PushChar();
			yield return new WaitForSeconds(LetterToLetterDelay);
		}
	}

	void PushChar()
	{
		Text_Show.text=""+CurrentWord;
	}
}
