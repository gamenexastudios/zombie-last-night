﻿using UnityEngine;
using MySystem;
using System.Collections;
using UnityEngine.UI;

public class VibrationBtn : MonoBehaviour 
{
	public static VibrationBtn myScript;

	public GameObject Btn_Vibration;

	public Sprite BtnTex_On,BtnTex_Off;

	public Text Text_Vibration;

	[HideInInspector]
	public string Is_Vibration;

	public string VibrationString;

	void Awake()
	{
		myScript=this;
		if (!PlayerPrefs.HasKey(VibrationString))
		{
			PlayerPrefs.SetString(VibrationString, "On");
		}
		Is_Vibration = PlayerPrefs.GetString(VibrationString);
		CheckVibration();
	}

	void Start () 
	{

	}

	void Update () 
	{

	}
	public void VibrationBtnAct()
	{
		if (Is_Vibration == "On")
		{
			Is_Vibration = "Off";
			PlayerPrefs.SetString(VibrationString, Is_Vibration);
			CheckVibration();
		}
		else
		{
			Is_Vibration = "On";
			PlayerPrefs.SetString(VibrationString, Is_Vibration);
			CheckVibration();
		}
	}
	public void CheckVibration()
	{
		if (Is_Vibration=="Off")
		{
			Btn_Vibration.GetComponent<Image>().sprite=BtnTex_Off;
			if (Text_Vibration != null)
			{
				Text_Vibration.text = "Vibration Off";
			}
		}
		else
		{
			if (Text_Vibration != null)
			{
				Text_Vibration.text = "Vibration On";
			}
			Btn_Vibration.GetComponent<Image>().sprite=BtnTex_On;
		}
	}

}
