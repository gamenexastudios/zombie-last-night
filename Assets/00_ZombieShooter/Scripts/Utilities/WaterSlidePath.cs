﻿using UnityEngine;
using MySystem;
using System.Collections;

public class WaterSlidePath : MonoBehaviour
{
	public static bool StartAnimation;
	public float AnimationSpeed=1f;
	public enum AnimDirection
	{
		Left,Right,Up,Down
	};
	public AnimDirection DirectionTo;
	public static float offset = 0;
	public int MaterialIndexValue=0;
	public bool Is_Start;

	void Start () 
	{
		offset=1;
	}
	
	void Update () 
	{
		if(!Is_Start)
			return;
		
		offset = Time.deltaTime*AnimationSpeed+offset;
		if(DirectionTo == AnimDirection.Up)
		{
			GetComponent<Renderer> ().materials [MaterialIndexValue].mainTextureOffset = new Vector2 (GetComponent<Renderer> ().material.mainTextureOffset.x, offset);
			if(offset >= 10 || offset <= -10)
				offset = 0;
		}
		else
		if(DirectionTo == AnimDirection.Down)
		{
			GetComponent<Renderer> ().materials [MaterialIndexValue].mainTextureOffset = new Vector2 (GetComponent<Renderer> ().material.mainTextureOffset.x, -offset);
			if(offset >= 10 || offset <= -10)
				offset = 0;	
		}
		else
		if(DirectionTo == AnimDirection.Right)
		{
			GetComponent<Renderer> ().materials [MaterialIndexValue].mainTextureOffset = new Vector2 (offset, GetComponent<Renderer> ().material.mainTextureOffset.y);
			if(offset >= 10 || offset <= -10)
				offset = 0;	
		}
		else
		if(DirectionTo == AnimDirection.Left)
		{
			GetComponent<Renderer> ().materials [MaterialIndexValue].mainTextureOffset = new Vector2 (-offset, GetComponent<Renderer> ().material.mainTextureOffset.y);
			if(offset >= 10 || offset <= -10)
				offset = 0;	
		}
	}
}
