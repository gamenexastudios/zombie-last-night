﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class ZombieBehavior : MonoBehaviour 
{
	public static ZombieBehavior _instance;

	public enum e_RigType
	{
		Humanoid,
		Generic
	}
	[Header("============ Set Rig Type ============")]
	public e_RigType m_RigType;
	public enum e_AnimState
	{
		None,
		Idle,
		Walk,
		Run,
		Attack,
		Hurt,
		Died,
		Jump
	}
	[Header("===================================================")]
	public bool _isFlyingType = false;
	[Space(20)]

	public e_AnimState m_Animstate;

	public enum e_ZombieBehavior
	{
		AttackPlayer,
		AttackOthers
	}
	public e_ZombieBehavior m_ZombieBehavior;
	public bool mb_RespondinStart;
	public Animator m_Animator;
	public NavMeshAgent m_Agent;
	public float mf_WalkSpeed, mf_RunSpeed;
	public GameObject mg_Player;
	public Rigidbody[] _rigid;
	[Header("=========== Jump Properties ============")]
	public bool mb_JumpBehave,mb_CrawlBehave;
	public Transform mt_JumpPos;
	public float _fJumpDelay;

	[Header("============ Health Properties ============")]
	public float mf_MaxHealth;
	public float mf_Currenthealth;
	[Header("============ Sounds Region ============")]
	public AudioSource m_AudioSource;
	public AudioClip Sound_Attack;
	public AudioClip Sound_Die,Sound_BloodSplash;
	public AudioClip[] Sound_Hurt;
	void Awake()
	{
		_instance = this;
	}
	void Start () 
	{
		mf_Currenthealth = mf_MaxHealth;
		m_Animator = GetComponent<Animator> ();
		m_AudioSource = GameManager._instance.ZombieAudio;
		if (_isFlyingType)
			return;
        _fAttackDist = 6f;
        m_Agent = GetComponent<NavMeshAgent> ();
		Check_InitialBehavior ();
		if(mb_JumpBehave)
		{
			StartCoroutine (JumptoGround (_fJumpDelay));
		}
		_rigid = GetComponentsInChildren<Rigidbody> ();

		foreach (Rigidbody _rb in _rigid)
			_rb.isKinematic = true;

//		for(int i=0;i<mesh_Zombie.Length;i++)
//		{
//			mesh_Zombie [i].material.color -= 0.1f;
//			mesh_Zombie [i].material.SetColor ("_Color", _clr);
////			Color _clr = mat_Zombie [i].color; 
////			_clr.a = 1f;
////			mat_Zombie [i].SetColor ("_Color", _clr);
//
//		}

		SetAnimState (m_Animstate);

		Resources.UnloadUnusedAssets();
		System.GC.Collect();
	}

	void Check_InitialBehavior()
	{
		mg_Player = GameManager._instance.mg_PlayerIns;
		if(m_Animstate == e_AnimState.Walk)
		{
			if(m_ZombieBehavior==e_ZombieBehavior.AttackPlayer)
				m_Agent.SetDestination (mg_Player.transform.position);
			else
			{
				_char = GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_AttackObj.GetComponent<CharBehavior> ();
				m_Agent.SetDestination (_char.transform.position);
			}

		}
		if(mb_RespondinStart)
		{
			SetAlert ();
		}
	}
	void RoamBehavior()
	{
		Vector3 randomDirection = Random.insideUnitSphere * 10;
		randomDirection += transform.position;
		NavMeshHit hit;
		NavMesh.SamplePosition(randomDirection, out hit, 10, 1);
		Vector3 finalPosition = hit.position;
		m_Agent.destination = finalPosition;
	}

	IEnumerator JumptoGround(float _delay)
	{
		yield return new WaitForSeconds (_delay);
		SetAnimState (e_AnimState.Jump);
		m_Agent.speed = 5f;
		m_Agent.SetDestination (mt_JumpPos.position);
		yield return new WaitForSeconds (1.4f);
		mb_JumpBehave = false;
		SetAlert ();
	}

	bool _isAttack;
	void Update () 
	{
		if(_canFade)
		{
			for(int i=0;i<mesh_Zombie.Length;i++)
			{
				Color _clr = mesh_Zombie [i].material.color;
				if(_clr.a>0) {
					_clr.a -= 0.01f;
					mesh_Zombie [i].material.SetColor ("_Color", _clr);
				}
				if(_clr.a<=0 && gameObject!=null)
				{
					Destroy (gameObject, 0.1f);
				}
			}
		}
		if (GameManager._instance.m_GameState != GameManager.e_GameState.GamePlay)
			return;
		
		if (_isFlyingType)
			return;


		if ( mf_Currenthealth <= 0)
			return;
        if (m_ZombieBehavior == e_ZombieBehavior.AttackPlayer)
        {
            _fCurrentDist = Vector3.Distance(transform.position, mg_Player.transform.position);
        }
        else 
        {
            if(_char!=null)
            _fCurrentDist = Vector3.Distance(transform.position, _char.transform.position);
        }

        if (m_Agent.enabled && _isAlerted)
		{
			if(mb_CrawlBehave)
			{
				m_Agent.stoppingDistance = 2f;
			}
			//if(m_Agent.remainingDistance <= m_Agent.stoppingDistance) //New Cmnted
            if(_fCurrentDist<=_fAttackDist)
			{
				if(m_ZombieBehavior == e_ZombieBehavior.AttackPlayer)
				{
					if(!_isAttack)
					{
						_isAttack = true;
						SetAnimState (e_AnimState.Attack);
					}
				}
				else
				{
					if(!_isAttack)
					{
						_isAttack = true;
						SetAnimState (e_AnimState.Attack);
					}
				}
			}
			else
			{
				_isAttack = false;
			}
		}
        //if(!mb_JumpBehave && m_Agent.remainingDistance <= m_Agent.stoppingDistance && m_Animstate!=e_AnimState.Idle && !_isAlerted)
        if (!mb_JumpBehave && _fCurrentDist<=_fAttackDist && m_Animstate != e_AnimState.Idle && !_isAlerted)

        {
            _isAlerted = true;
		}

	}

    public float _fAttackDist=2f;

    bool _canFade;
	bool _isAlerted;
	CharBehavior _char;
	public void SetAlert()
	{
		if (_isFlyingType)
			return;

		if (m_Animstate != e_AnimState.Run && !mb_RespondinStart && m_Animstate!=e_AnimState.Attack) 
		{
			if(!mb_JumpBehave)
				SetAnimState (e_AnimState.Run);
		}
		if(!_isAlerted && !mb_JumpBehave)
		{
			if(m_Agent)
			{
				if( m_Agent.enabled && m_ZombieBehavior==e_ZombieBehavior.AttackPlayer && mf_Currenthealth>0)
				{
					m_Agent.SetDestination (mg_Player.transform.position);
					int _rand = Random.Range (0, 2);
					if(_rand==0)
						SetAnimState (e_AnimState.Run);
					else
						SetAnimState (e_AnimState.Walk);
				}
				else if(m_ZombieBehavior==e_ZombieBehavior.AttackOthers && m_Agent.enabled)
				{
					_char = GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_AttackObj.GetComponent<CharBehavior> ();

					m_Agent.SetDestination (GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mt_AttackObj.position);
					SetAnimState (e_AnimState.Run);
				}
				Invoke ("MakeAlertTrue", 0.5f);
			}

		}
	}

	void MakeAlertTrue()
	{
//		Debug.Log ("------ Alerted");
		_isAlerted = true;
	}
	public void SetAnimState(e_AnimState _state)
	{
		m_Animstate = _state;
//		if (GameManager._instance.m_GameState != GameManager.e_GameState.GamePlay)
//			return;
		
		if(m_RigType==e_RigType.Humanoid)
		{
			if (m_Agent == null)
				return;
			if(m_Animstate==e_AnimState.Walk)
			{
				if(mb_CrawlBehave)
				{
					m_Agent.speed = mf_WalkSpeed;
					m_Animator.SetInteger ("AnimCount", 20);
				}
				else {
					int _rand = Random.Range (5, 8);
					m_Agent.speed = mf_WalkSpeed;
					m_Animator.SetInteger ("AnimCount", _rand);
				}
			}
			if(m_Animstate==e_AnimState.Run)
			{
				if(mb_CrawlBehave)
				{
					m_Agent.speed = mf_WalkSpeed+0.5f;
					m_Animator.SetInteger ("AnimCount", 20);
				}
				else
				{
					int _rand = Random.Range (8, 10);
					m_Agent.speed = mf_RunSpeed;
					m_Animator.SetInteger ("AnimCount", _rand);
				}
			}
			if(m_Animstate==e_AnimState.Jump)
			{
				int _rand=Random.Range(10,13);
				m_Animator.SetInteger ("AnimCount", _rand);
			}
			if(m_Animstate==e_AnimState.Attack)
			{
				if(mb_CrawlBehave)
				{
					m_Animator.SetInteger ("AnimCount", 21);
				}
				else{
					int _rand = Random.Range (13, 15);
					m_Animator.SetInteger ("AnimCount", _rand);
				}
			}
			if(m_Animstate==e_AnimState.Hurt)
			{
				if(!mb_CrawlBehave) {
					int _rand = Random.Range (16, 18);
					m_Animator.SetInteger ("AnimCount", _rand);
				}
				else
					m_Animator.SetInteger ("AnimCount", 23);

			}
			if(m_Animstate==e_AnimState.Died)
			{
				//m_AudioSource.clip = Sound_Die;
				m_AudioSource.PlayOneShot(Sound_Die);
				if(GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.m_LevelTargets.Length-1].mlist_Zombies.Count==1)
				{
//					Debug.Log ("---- Here Showing Cam Effect");
//					GameManager._instance.Enable_FinalEffect (this.transform.position);
				}
				if(!_isFlyingType || !mb_CrawlBehave) 
				{
					m_Agent.enabled = false;
					foreach (Rigidbody _rb in _rigid) {
						_rb.isKinematic = false;
						Vector3 _dir = transform.position - GameManager._instance.mg_PlayerIns.transform.position;
						_rb.AddForceAtPosition (_dir.normalized * Random.Range(500,750), transform.position);

						if(transform.gameObject.GetComponent<NavMeshObstacle> ()==null)
							transform.gameObject.AddComponent<NavMeshObstacle> ();
					}
				}
				if(mb_CrawlBehave)
				{
					m_Animator.SetInteger ("AnimCount", 22);
				}
				m_Animator.enabled=false;
				if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Career || StaticVariables.m_GameMode==StaticVariables.e_GameMode.Bounty)
				{
					GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mlist_Zombies.Remove (this);
					GameManager._instance.scr_LevelInfo.Check_LC ();
				}
				else if(StaticVariables.m_GameMode==StaticVariables.e_GameMode.Survival)
				{
					SurvivalMode._instance.mlist_Zombies.Remove (this);
					SurvivalMode._instance.Check_ZombieCount ();
					Debug.Log ("---- Survival Mode ZombiesCount::::" + SurvivalMode._instance.mlist_Zombies.Count);
				}
			}
		}
		else
		{
			if(m_Animstate==e_AnimState.Walk)
			{
				m_Agent.speed = mf_WalkSpeed;
				m_Animator.SetInteger ("AnimCount", 1);
			}
			if(m_Animstate==e_AnimState.Run)
			{
				m_Agent.speed = mf_RunSpeed;
				m_Animator.SetInteger ("AnimCount", 2);
			}
			if(m_Animstate==e_AnimState.Jump)
			{
				m_Animator.SetInteger ("AnimCount", 4);
			}
			if(m_Animstate==e_AnimState.Attack)
			{
                if (GameManager._instance.m_GameState == GameManager.e_GameState.GamePlay)
                {
                    //m_AudioSource.clip = Sound_Attack;
                    m_AudioSource.PlayOneShot(Sound_Attack);
                    Debug.Log("Attack Sound");
                }

				if(!_isFlyingType)
					m_Animator.SetInteger ("AnimCount", 5);
				else {
					m_Animator.SetInteger ("AnimCount", 1);
				}
                
			}
			if(m_Animstate==e_AnimState.Hurt)
			{
				m_Animator.SetInteger ("AnimCount", 3);
			}

			if(m_Animstate==e_AnimState.Died)
			{
				//m_AudioSource.clip = Sound_Die;
				m_AudioSource.PlayOneShot(Sound_Die);
				if(GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.m_LevelTargets.Length-1].mlist_Zombies.Count==1 && !_isFlyingType)
				{
//					GameManager._instance.Enable_FinalEffect (this.transform.position);
				}
				if (!_isFlyingType) 
				{
//					Debug.Log ("---- Dog DEAD");
					m_Animator.SetInteger ("AnimCount", 100);
//					Debug.LogError ("---- Dog DEAD");
				}
				else
				{
					m_Animator.SetInteger ("AnimCount", 2);
					GetComponent<SWS.splineMove> ().Stop ();
					gameObject.AddComponent<Rigidbody> ();
					gameObject.GetComponent<Rigidbody> ().mass = 0.1f;
				}
				GameManager._instance.scr_LevelInfo.m_LevelTargets [GameManager._instance.scr_LevelInfo.mi_CurrentWave].mlist_Zombies.Remove (this);
				GameManager._instance.scr_LevelInfo.Check_LC ();
			}
		}
	}

	Ray _ForceRay = new Ray ();
	RaycastHit _ForceHit = new RaycastHit ();

	public void ApplyDamage(float _val=0,int _shotnum = 10,Ray _ray= new Ray() ,RaycastHit _hit= new RaycastHit())
	{
		int rnd = Random.Range (0, Sound_Hurt.Length);
		//m_AudioSource.clip = Sound_Hurt[rnd];
		m_AudioSource.PlayOneShot(Sound_Hurt[rnd]);
		if(mf_Currenthealth>0)
		{
			if(m_RigType!=e_RigType.Generic) 
			{
					mf_Currenthealth -= _val;
			}
			else 
			{
				mf_Currenthealth -= _val / 2;// 5;
			}
			if(!m_Animstate.Equals(e_AnimState.Attack))
			{
				if(!_isFlyingType) {
					m_Agent.speed = 0;
					SetAnimState (e_AnimState.Hurt);
					if(!mb_CrawlBehave)
						Invoke ("SetRunState", 0.7f);
					else
						Invoke ("SetRunState", 0.25f);
				}
			}
        }

		if(mf_Currenthealth<=0)
		{
			mf_Currenthealth = 0;
			SetAnimState (e_AnimState.Died);
			if(_shotnum==0)
			{
				InGameUIHandler._instance.Show_ShotName (0);
			}
			_ForceHit = _hit;
			_ForceRay = _ray;
			GameManager._instance.scr_LevelInfo.mi_DoneCount += 1;
			InGameUIHandler._instance.Update_TargetText (GameManager._instance.scr_LevelInfo.mi_DoneCount);
			if (GetComponent<MapMarker> ()) 
			{
				GetComponent<MapMarker> ().enabled = false;
				GetComponent<MapMarker> ().isActive = false;
			}
			_canFade=true;
		}
	}
	[Header("================= Material ================")]
//	public Material[] mat_Zombie;
	public SkinnedMeshRenderer[] mesh_Zombie;
    public float _fCurrentDist;
	void SetRunState()
	{
		if(mf_Currenthealth>0)
		{
			SetAnimState (e_AnimState.Run);
			if (m_Agent.enabled==true && _fCurrentDist<=_fAttackDist)// && m_Agent.remainingDistance <= m_Agent.stoppingDistance)
				SetAnimState (e_AnimState.Attack);
		}
	}
	public void DamagetoPlayer()
	{
		if (_isFlyingType) {
			SetAnimState (e_AnimState.Attack);

		}
		if(m_ZombieBehavior==e_ZombieBehavior.AttackPlayer)
		{
			if(playercontroller._instance.mf_PlayerCurrentHealth>0 && GameManager._instance.m_GameState==GameManager.e_GameState.GamePlay)
			{
				if(m_RigType!=e_RigType.Generic)
					playercontroller._instance.ApplyDamage (35);
				else 
				{
					if(!_isFlyingType)
						playercontroller._instance.ApplyDamage (101);
					else
						playercontroller._instance.ApplyDamage (30);
				}

                //m_AudioSource.clip = Sound_Attack;
                m_AudioSource.PlayOneShot(Sound_Attack);
                Debug.Log("Attack Sound");
            }
		}
		else
		{
			if(_char._fCurrentHealth>0)
			{
				_char.ApplyDamagetoOthers (8);
                Debug.Log("Attacking Other Char");
			}
			else
			{
				m_Animstate = e_AnimState.Idle;
				m_Animator.SetInteger ("AnimCount", 1);
			}
		}
	}

	public void Call_SlowMotion(bool _status)
	{
		if(_status)
		{
			if(m_Animstate!=e_AnimState.Died)
			{
				m_Animator.speed = 0.5f;
				if(!_isFlyingType)
					m_Agent.speed = 1f;
			}
		}
		else
		{
			if(m_Animstate!=e_AnimState.Died)
			{
				if(m_Animator)
					m_Animator.speed = 1f;
				if(m_Agent)
				{
					if (m_Animstate == e_AnimState.Run)
						m_Agent.speed = mf_RunSpeed;
					else if (m_Animstate == e_AnimState.Walk)
						m_Agent.speed = mf_WalkSpeed;
				}
			}
		}
	}

	public void Call_SoundAttack()
	{
		//m_AudioSource.clip = Sound_Attack;
		m_AudioSource.PlayOneShot(Sound_Attack);
	}
}
