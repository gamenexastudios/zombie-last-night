﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ZombieCreate : MonoBehaviour 
{
	public enum e_RigType
	{
		Humanoid,Generic
	}
	public e_RigType m_RigType;
	public string _sZombieName;
	public enum e_InitialState
	{
		Idle,
		Walk,
		Run
	}
	[Header("======================================================")]

	public e_InitialState m_InititalState;

	public enum e_MainBehavior
	{
		AttackPlayer,
		AttackOthers
	}
	public e_MainBehavior m_MainBehavior;

	public bool _isRespondinStart,_isFlyingType;
	public bool _canJump,_canCrawl;
	public Transform _tJumpPos;
	GameObject _gZombie;
	ZombieBehavior scr_Zombie;
	public float _fJumpDelay;
	[Header("========== Flying Zombie Properties ============")]
	public SWS.PathManager FlyingPath;
	public float _fFlyingTime;
	string _zombie;
	int rand;

	void Awake () 
	{
		if (m_RigType == e_RigType.Humanoid) 
		{
//			_gZombie = Instantiate (Resources.Load ("Zombie_" + Random.Range (1, 8)), transform.position, transform.rotation)as GameObject;
			int _rnd=Random.Range(0,InGameUIHandler._instance.mlist_ZombiePrefabs.Count);
			_gZombie = Instantiate (InGameUIHandler._instance.mlist_ZombiePrefabs[_rnd], transform.position, transform.rotation)as GameObject;

		}
		else
		{
			if (_sZombieName != null) 
			{
				for(int i=0;i<InGameUIHandler._instance.mlist_SplZombiePrefabs.Count;i++)
				{
					if(_sZombieName==InGameUIHandler._instance.mlist_SplZombiePrefabs[i].name)
					{
						_zombie = InGameUIHandler._instance.mlist_SplZombiePrefabs [i].name;
						rand = i;
//						break;

					}
				}
				Debug.Log ("---- Spl Zombie Ins:::: " + _zombie  +"  ---- ID ::::"+ rand);
//				_gZombie = Instantiate (Resources.Load (_sZombieName), transform.position, transform.rotation)as GameObject;
				_gZombie = Instantiate (InGameUIHandler._instance.mlist_SplZombiePrefabs[rand], transform.position, transform.rotation)as GameObject;
			}

			else
				Debug.Log ("<color=red> Please Give Name for SpecialZombie!!! </color>");
		}

		_gZombie.transform.SetParent (this.transform);

		scr_Zombie = _gZombie.GetComponent<ZombieBehavior> ();
		scr_Zombie.mb_RespondinStart =_isRespondinStart;
		scr_Zombie._isFlyingType = _isFlyingType;
		scr_Zombie.mb_JumpBehave = _canJump;
		scr_Zombie.mb_CrawlBehave = _canCrawl;
		if(_isFlyingType)
		{
			scr_Zombie.gameObject.GetComponent<SWS.splineMove> ().pathContainer = FlyingPath;
			scr_Zombie.gameObject.GetComponent<SWS.splineMove> ().StartMove ();
			scr_Zombie.gameObject.GetComponent<SWS.splineMove> ().speed = _fFlyingTime;
		}

		if(scr_Zombie.mb_JumpBehave)
		{
			scr_Zombie.mt_JumpPos = _tJumpPos;
			scr_Zombie._fJumpDelay = _fJumpDelay;
			_gZombie.GetComponent<OffMeshLink> ().startTransform = GetComponent<OffMeshLink>().startTransform;
			_gZombie.GetComponent<OffMeshLink> ().endTransform = GetComponent<OffMeshLink>().endTransform;
		}
			


		if (m_InititalState == e_InitialState.Idle)
			scr_Zombie.m_Animstate = ZombieBehavior.e_AnimState.Idle;
		else if(m_InititalState == e_InitialState.Walk)
			scr_Zombie.m_Animstate = ZombieBehavior.e_AnimState.Walk;
		else if(m_InititalState == e_InitialState.Run)
			scr_Zombie.m_Animstate = ZombieBehavior.e_AnimState.Run;

		if (m_MainBehavior == e_MainBehavior.AttackPlayer)
			scr_Zombie.m_ZombieBehavior = ZombieBehavior.e_ZombieBehavior.AttackPlayer;
		else
			scr_Zombie.m_ZombieBehavior = ZombieBehavior.e_ZombieBehavior.AttackOthers;
	}
}
