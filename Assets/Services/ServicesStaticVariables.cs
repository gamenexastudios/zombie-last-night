﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServicesStaticVariables : MonoBehaviour
{
    public static int _firstTime;
    public static ServicesStaticVariables _instance;

    public static bool _homeButtonPressed;
    public static bool _retryButtonPressed;



    public static string level1 = "level1";
    public static string level2 = "level2";
    public static string level3 = "level3";
    public static string level4 = "level4";
    public static string level5 = "level5";
    public static string level6 = "level6";
    public static string level7 = "level7";
    public static string level8 = "level8";
    public static string level9 = "level9";
    public static string level10 = "level10";
    public static string level11 = "level11";
    public static string level12 = "level12";
    public static string level13 = "level13";
    public static string level14 = "level14";
    public static string level15 = "level15";
    public static string level16 = "level16";
    public static string level17 = "level17";
    public static string level18 = "level18";
    public static string level19 = "level19";
    public static string level20 = "level20";


    


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(this);

    }



}



