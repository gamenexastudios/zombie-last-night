﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScree : MonoBehaviour
{
    [Header("=================SPLASH SCREEN IMAGE====================")]
    [SerializeField] GameObject _splashImage;
    [SerializeField] float _displayTime = 8.0f;
    [SerializeField] bool _testMode = false;
    [SerializeField] GameObject _2xRewardPannel;
    [SerializeField] GameObject _closeButton;

    // Start is called before the first frame update
    void Start()
    {
        if (ServicesStaticVariables._firstTime == 0)
        {
            _splashImage.SetActive(true);
        }
        else
            _splashImage.SetActive(false);


        if(_testMode)
        {
            PlayerPrefs.SetInt("UnlockedLevelsA", 20);
            StaticVariables.strUnlockedWeapons = StaticVariables.strUnlockedWeaponsRef;
            if (PlayerPrefs.GetInt("OnlyOnce", 0) == 0)
            {
                SceneManager.LoadScene("Menu");
                PlayerPrefs.SetInt("OnlyOnce", 1);
            }
        }

       // GoogleAdMobsManager.instance.RequestRewardVideo();
       // GoogleAdMobsManager.instance.RequestInterstitialAd();
        //GoogleAdMobsManager.instance.RequestBannerAd();
        StartCoroutine(SplashScreenDisplay());
    }

    IEnumerator SplashScreenDisplay()
    {
        yield return new WaitForSeconds(8.0f);
        if (ServicesStaticVariables._firstTime == 0)
        {
            //GoogleAdMobsManager.instance.DisplayInterstitial_Launch();
            ServicesStaticVariables._firstTime = 1;
        }
        _splashImage.SetActive(false);
        yield return new WaitForSeconds(6.0f);
        //GoogleAdMobsManager.instance.DisplayBanner();
    }

    public void close2xPannel()
    {
        _2xRewardPannel.SetActive(false);
    }

    public void ActivateCloseButton()
    {
        _closeButton.SetActive(true);
    }

}
